(in-package :bloomie)

(defstruct (recommendation (:conc-name reco-))
  ticker as-of firm-name analyst recommendation rating
  action-code target-price period date barr one-year-return)

(defun stock-recommendations (ticker &optional as-of)
  "Returns a list of lists of the form (firm analyst recommendation rating code target period date barr ret1yr)"
  (let ((raw (bdp (format nil "~A Equity" ticker) "BEST_ANALYST_RECS_BULK"
		  :overrides (when as-of `(("END_DATE_OVERRIDE" ,(bbdate as-of)))))))
    (if (second (first raw))
	(unify:match ('((_ #T(list :BEST_ANALYST_RECS_BULK &rest ?recos))) raw)
	  (loop for reco in recos
	     collect (unify:match ('(:BEST_ANALYST_RECS_BULK (:|Firm Name| ?firm)
				     (:|Analyst| ?analyst) (:|Recommendation| ?recommendation)
				     (:|Rating| ?rating) (:|Action Code| ?code)
				     (:|Target Price| ?target) (:|Period| ?period)
				     (:|Date| . ?date) (:BARR ?barr) (:|1 Year Return| ?ret1yr))
				    reco)
		       (make-recommendation :ticker ticker :as-of as-of
					    :firm-name firm :analyst analyst
					    :recommendation recommendation
					    :rating (if (= rating *na-int*) nil rating)
					    :action-code code
					    :target-price (if (= target *na-double*) nil target)
					    :period period
					    :date  (or (first date) "1900-01-01")
					    :barr (if (= barr *na-int*) nil barr)
					    :one-year-return (if (= ret1yr *na-double*) nil ret1yr)))))
	(error (third (first raw))))))
    
