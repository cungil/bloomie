(in-package :bloomie)

(defvar *yellow-keys* '(:none :cmdt :eqty :muni :prfd :clnt :mmkt :govt :corp :indx :curr :mtge))

(defvar *language-overrides* '(:none :english :french :german :spanish :portuguese :italian
			       :russian :kanji :korean :chinese_trad :chinese_simp))

(defun instruments (query &key (max-results 1000) (yellow-key :none) (language-override :none))
  "Retrieve a list of instruments of the given type (language-override seems to have no effect?)"
  (when (stringp yellow-key) (setf yellow-key (intern (string-upcase yellow-key) "KEYWORD")))
  (when (stringp language-override) (setf language-override (intern (string-upcase language-override) "KEYWORD")))
  (assert (member yellow-key *yellow-keys*))
  (assert (member language-override *language-overrides*))
  (let* ((request (request "instrumentList" :query query :language-override (format nil "LANG_OVERRIDE_~A" :none)
			   :max-results max-results :yellow-key (concatenate 'string "YK_FILTER_" (symbol-name yellow-key))))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    response))

(defun govt (query &key (max-results 1000) ticker (partial-match t))
  "Retrieve a list of government bonds, if ticker is given the search is restricted to that issuer (if partial-match is nil).
If partial-match is true (default), all the issuers with tickers starting like the one specified are included."
  (let* ((request (request "govtList" :query query :max-results max-results :ticker ticker :partial-match partial-match))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    response))

(defvar *curve-types* '(:invalid :unassigned :irs :govt :agency :muni :corp :mtge :mmkt :curncy :comdty))

(defvar *curve-subtypes* '(:invalid :unassigned :senior :subordinated :zero :ois :inflation :spread :cds :rate :sector :issuer))

(defstruct (curve (:conc-name curve-))
  ticker description country currency type subtype publisher curve-id bbg-id)

(defun curve (query &key (max-results 1000) country-code currency-code type subtype curve-id bbg-id)
  "Retrieve a list of government bonds, if ticker is given the search is restricted to that issuer (if partial-match is nil).
If partial-match is true (default), all the issuers with tickers starting like the one specified are included."
  (when (and country-code (symbolp country-code)) (setf country-code (symbol-name country-code)))
  (when (and currency-code (symbolp currency-code)) (setf currency-code (symbol-name currency-code)))
  (when (and type (stringp type)) (setf type (intern (string-upcase type) "KEYWORD")))
  (when (and subtype (stringp subtype)) (setf subtype (intern (string-upcase subtype) "KEYWORD")))
  (when (and curve-id (symbolp curve-id)) (setf curve-id (symbol-name curve-id)))
  (when (and bbg-id (symbolp bbg-id)) (setf bbg-id (symbol-name bbg-id)))
  (assert (or (null country-code) (and (stringp country-code) (= 2 (length country-code)))))
  (assert (or (null currency-code) (and (stringp currency-code) (= 3 (length currency-code)))))
  (assert (or (null type) (member type *curve-types* :test #'string=)))
  (assert (or (null subtype) (member subtype *curve-subtypes* :test #'string=)))
  (assert (or (null curve-id) (stringp curve-id)))
  (assert (or (null bbg-id) (stringp bbg-id)))
  (let* ((request (request "curveList" :query query :max-results max-results :country-code country-code :currency-code currency-code
			   :curve-type (symbol-name type) :curve-subtype (symbol-name subtype) :curve-id curve-id :bbg-id bbg-id))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    (loop for (ticker description country currency curve-id type subtype publisher bbg-id) in response
       collect (make-curve :ticker ticker :description description :country country :currency currency
			   :curve-id curve-id :type type :subtype subtype :publisher publisher :bbg-id bbg-id))))
