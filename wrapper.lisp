(in-package :bloomie.ffi)

;;; CFFI

;; to regenerate the autowrap definitions, delete or rename the .spec files
;; requires LLVM/Clang and c2ffi (https://github.com/rpav/c2ffi)

#-lispworks (autowrap:c-include '(bloomie ffi "blpapi-3.11.1.1.h") :spec-path '(bloomie ffi))

;;; FLI

;; to regenerate the definitions for LispWorks

;; (cl:load (cl:merge-pathnames "ffi/parse-blpapi.lisp" (asdf:system-source-directory :bloomie)))

;; this will generate a blpapi-dff.raw file, create the .lisp file making the following changes
;;
;;  (:pointer (:pointer  =>  (:reference-return (:pointer
;;     ATTENTION: sometimes the first and the second :pointer are in different lines!
;;     manually for blpapi-operation-request-definition, blpapi-operation-response-definition,
;;     blpapi-service-get-operation[-at], blpapi-service-get-event-definition[-at]
;;
;;  (:pointer (:const :char))  =>  (:reference :ef-mb-string :allow-null t)
;;
;;  (buffer (:pointer  =>  (buffer (:reference-return
;;     ATTENTION: sometimes :buffer and :pointer are in different lines!
;;     manually for blpapi-element-get-value-as-high-precision-datetime,
;;     blpapi-constant-get-value-as-string, blpapi-element-get-value-as-string

#+lispworks (fli:set-locale-encodings :utf-8 nil)
#+lispworks (cl:load (cl:merge-pathnames "ffi/blpapi-dff.lisp" (asdf:system-source-directory :bloomie)))

(cl:in-package :bloomie)

#-lispworks
(defun version-info ()
  (plus-c:c-with ((major :int)
		  (minor :int)
		  (patch :int)
		  (build :int))
		 (bb.ffi:blpapi-get-version-info
		  (major plus-c:&) (minor plus-c:&) (patch plus-c:&) (build plus-c:&))
		 (list major minor patch build)))

#+lispworks
(defun version-info ()
  (fli:with-dynamic-foreign-objects ((major :int) (minor :int)
				     (patch :int) (build :int))
    (bb.ffi::blpapi-get-version-info major minor patch build)
    (list (fli:dereference major) (fli:dereference minor)
	  (fli:dereference patch) (fli:dereference build))))

#+lispworks
(defun blpapi-library-loaded ()
  (fli:connected-module-pathname :blpapi))
	       
#-lispworks
(defun blpapi-library-loaded ()
  (loop for lib in (cffi:list-foreign-libraries)
     for name = (symbol-name (cffi:foreign-library-name lib))
     when (and (> (length name) 6) (string= (subseq name 0 6) "BLPAPI"))
     collect lib))

(defun close-blpapi-library (&optional force-closing-on-clozurecl)
  ;; Closing the dll could crash the system if there are some unallocated foreign objects.
  (let ((loaded (blpapi-library-loaded)))
    (when loaded
      (close-session)
      #+lispworks (progn
		    (format t "~%closing blpapi ~A" loaded)
		    (fli:disconnect-module :blpapi :remove t))
      #-lispworks (loop for lib in loaded
		     do (format t "~%closing blpapi ~A" (cffi:foreign-library-pathname lib))
		       #+ccl (format t "Warning: the library cannot be reloaded after being closed on ClozureCL")
		       (cffi:close-foreign-library lib)))))

(defun load-blpapi-library (&key if-loaded)
  (if (blpapi-library-loaded)
      (case if-loaded
	(:ignore)
	(:close-and-reload (close-blpapi-library) (load-blpapi-library))
	(t (warn "The library is already loaded. To unload/reload the dll (not supported on ClozureCL) do (load-blpapi-library :if-loaded :close-and-reload)")))
      (let ((dll #-lispworks (cffi:load-foreign-library #-x86-64 "blpapi3_32.dll"
							#+x86-64 "blpapi3_64.dll")
		 #+lispworks (fli:register-module :blpapi :real-name
						  #-x86-64 "blpapi3_32.dll"
						  #+x86-64 "blpapi3_64.dll")))
	(destructuring-bind (major minor patch build) (version-info)
	  (format t "~%loaded blpapi ~A.~A.~A.~A ~A" major minor patch build
		  #+lispworks (fli:connected-module-pathname dll)
		  #-lispworks (cffi:foreign-library-pathname dll))
	  (if (< (+ (* 10000 major) (* 100 minor) patch) 31008)
	      (format t "~%~%  blpapi 3.10.7 or later recommended, this version might not work properly.~%  If you have a more recent dll which is not being found you can set the full path in wrapper.lisp~%  You can download the library from https://www.bloomberg.com/professional/support/api-library/ or WAPI<GO>~%~%")))
	dll)))
