(in-package :bloomie)

(defun ibar (security event-type &key start-date-time end-date-time (bar-size '(30 :min)) overrides)
  "Retrieve intraday bars of the given size. Event type should be one of TRADE, BID, ASK, BEST_BID, or BEST_ASK.
Bar size (30 minutes by default) can be given as a number (in minutes) or as a list like (30 :min) or (1 :h).
Start date/time and end date/time (required) should be like yyyy-mm-dd or yyyy-mm-ddThh:mm:ss.
Overrides is an optional list with elements of the form (\"KEY\" \"VALUE\")."
  (when (symbolp event-type) (setf event-type (symbol-name event-type)))
  (assert (member event-type '("TRADE" "BID" "ASK" "BEST_BID" "BEST_ASK") :test #'string=))
  (let* ((interval (if (numberp bar-size)
		       bar-size
		       (ecase (elt (symbol-name (second bar-size)) 0)
			 (#\M (first bar-size))
			 (#\H (* 60 (first bar-size))))))
	 (request (request "IntradayBar" :security security :event-type event-type :interval interval
			   :start-date-time start-date-time :end-date-time end-date-time :overrides overrides)))
    #+lispworks (let ((mb (mp:make-mailbox)))
		  (setf (gethash (send-request request) *mailboxes*) mb)
		  (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
    #-lispworks (progn (send-request request) (retrieve-response))))
