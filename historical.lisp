(in-package :bloomie)

(defun bdh (securities fields &key (periodicity :daily) (start-date (n-days-ago 10)) end-date overrides)
  "Allows for multiple securities and fields. Periodicity is :daily by default. 
Start date and end date should be like YYYY-MM-DD, if none is given the default start date is 10 days ago.
Overrides is an optional list with elements of the form (\"KEY\" \"VALUE\")."
  (assert (member periodicity '(:daily :weekly :monthly :quarterly :semi-anually :yearly)))
  (let ((request (request "HistoricalData" :securities securities :fields fields
			  :start-date start-date :end-date end-date :periodicity periodicity :overrides overrides)))
    #+lispworks (let ((mb (mp:make-mailbox)))
		  (setf (gethash (send-request request) *mailboxes*) mb)
		  (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
    #-lispworks (progn (send-request request) (retrieve-response))))
