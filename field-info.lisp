(in-package :bloomie)

(defstruct (category (:conc-name category-))
  name id num-fields description is-leaf-node fields)

(defstruct (field (:conc-name field-))
  mnemonic id description overrides documentation category-name datatype ftype)

(defun field-error (field)
  (let ((field-error (ignore-errors (unify:match (#T(list (:|id| ?id)
							  (:|fieldError| 
							    (:|source| ?source)
							    (:|code| ?code)
							    (:|category| ?category)
							    (:|message| ?message)))
						    field)
				      (list id source code category message)))))
    (when field-error
      (error "~A - ~A" (first field-error) (fifth field-error)))))

(defvar *field-types* '((:all . "All")
			(:real-time . "RealTime")
			(:static . "Static")))

(defun field-list (&key (type :all) (documentation nil))
  "Return the whole list of fields. Can be restricted to :static or :real-time (:all by default).
The keyword argument :documentation can be set to true (nil by default)."
  (assert (member type (mapcar #'first *field-types*)))
  (let* ((request (request "FieldList" :field-type (cdr (assoc type *field-types*)) :return-documentation documentation))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    (let ((total (length response))
	  (unique (length (remove-duplicates (mapcar #'cadar response) :test #'string=))))
      (unless (= total unique) (warn "~A fields but only ~A unique" total unique)))
    (if documentation
	(loop for field in response
	   collect (unify:match (#T(list (:|id| ?id)
					 #T(list (:|mnemonic| ?mnemonic)
						 (:|description| ?description)
						 #T(list :|datatype| &rest ?datatype)
						 (:|documentation| ?documentation)
						 #T(list :|categoryName| &rest ?category-name)
						 (:|property|)
						 #T(list :|overrides| &rest ?overrides)
						 #T(list :|ftype| &rest ?ftype)))
				   field)
		     (make-field :id id
				 :mnemonic mnemonic
				 :description description
				 :overrides overrides
				 :documentation documentation
				 :category-name category-name
				 :datatype datatype
				 :ftype ftype)))
	(loop for field in response
	   collect (unify:match (#T(list (:|id| ?id)
					 #T(list (:|mnemonic| ?mnemonic)
						 (:|description| ?description)
						 #T(list :|datatype| &rest ?datatype)
						 #T(list :|categoryName| &rest ?category-name)
						 (:|property|)
						 #T(list :|overrides| &rest ?overrides)
						 #T(list :|ftype| &rest ?ftype)))
				   field)
		     (make-field :id id
				 :mnemonic mnemonic
				 :description description
				 :overrides overrides
				 :category-name category-name
				 :datatype datatype
				 :ftype ftype))))))

(defun document-fields (&optional skip-existing)
  "Retrive the documentation for every field and save it in the fields/ directory.
There seems to be a bug (server side?) that results in some fields appearing zero/multiple times.
The set of missing fields changes in different runs, run multiple times until completing the data 
(the optional skip-existing flag can be used to avoid rewriting the existing files)."
  (let ((all-fields (field-list :documentation t))
	#-lispworks (*print-pretty* t)) ;; chokes on non-ascii characters, at least with LW 6.0
    (loop for field in all-fields
       for id = (field-id field)
       for output-file = (merge-pathnames (format nil "fields/~A" id)
					  (asdf:system-source-directory :bloomie))
       for i from 1
       with total = (length all-fields)
       do (format t "~&~A~19T~6D / ~D~%" id i total)
	 (unless (and skip-existing (probe-file output-file))
	   (with-open-file (out output-file
				:direction :output :if-exists :supersede :external-format :utf-8)
	     (write field :stream out))))))

(defun field-info (field-ids &key (documentation t))
  "Return information for the given field or fields (id or mnemonic). 
The keyword argument :documentation can be set to nil (true by default)."
  (let* ((request (request "FieldInfo" :ids field-ids :return-documentation documentation))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    (loop for field in response
          collect (or (field-error field)
                      (if documentation 
                          (unify:match (#T(list (:|id| ?id)
                                                #T(list (:|mnemonic| ?mnemonic)
                                                        (:|description| ?description)
                                                        #T(list :|datatype| &rest ?datatype)
                                                        (:|documentation| ?documentation)
                                                        (:|categoryName|)
                                                        (:|property|)
                                                        #T(list :|overrides| &rest ?overrides)
                                                        #T(list :|ftype| &rest ?ftype)))
                                          field)
                            (make-field :id id
                                        :mnemonic mnemonic
                                        :description description
                                        :overrides overrides
                                        :documentation documentation
                                        :datatype datatype
                                        :ftype ftype))
                        (unify:match (#T(list (:|id| ?id)
                                              #T(list (:|mnemonic| ?mnemonic)
                                                      (:|description| ?description)
                                                      #T(list :|datatype| &rest ?datatype)
                                                      (:|categoryName|)
                                                      (:|property|)
                                                      #T(list :|overrides| &rest ?overrides)
                                                      #T(list :|ftype| &rest ?ftype)))
                                        field)
                          (make-field :id id
                                      :mnemonic mnemonic
                                      :description description
                                      :overrides overrides
                                      :datatype datatype
                                      :ftype ftype)))))))

(defun field-search (search-terms &key (documentation nil))
  "Search for fields matching the provided term(s).
The keyword argument :documentation can be set to true (nil by default)."
  (let* ((request (request "FieldSearch" :search search-terms :return-documentation documentation))
	 (response #+lispworks (let ((mb (mp:make-mailbox)))
				 (setf (gethash (send-request request) *mailboxes*) mb)
				 (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		   #-lispworks (progn (send-request request) (retrieve-response))))
    (if documentation
	(loop for field in response
	   collect (unify:match (#T(list (:|id| ?id)
					 #T(list (:|mnemonic| ?mnemonic)
						 (:|description| ?description)
						 #T(list :|datatype| &rest ?datatype)
						 (:|documentation| ?documentation)
						 #T(list :|categoryName| &rest ?category-name)
						 (:|property|)
						 #T(list :|overrides| &rest ?overrides)
						 #T(list :|ftype| &rest ?ftype)))
				   field)
		     (make-field :id id
				 :mnemonic mnemonic
				 :description description
				 :overrides overrides
				 :documentation documentation
				 :category-name category-name
				 :datatype datatype
				 :ftype ftype)))
	(loop for field in response
	   collect (unify:match (#T(list (:|id| ?id)
					 #T(list (:|mnemonic| ?mnemonic)
						 (:|description| ?description)
						 #T(list :|datatype| &rest ?datatype)
						 #T(list :|categoryName| &rest ?category-name)
						 (:|property|)
						 #T(list :|overrides| &rest ?overrides)
						 #T(list :|ftype| &rest ?ftype)))
				   field)
		     (make-field :id id
				 :mnemonic mnemonic
				 :description description
				 :overrides overrides
				 :category-name category-name
				 :datatype datatype
				 :ftype ftype))))))

;; APITerminalFieldSearch seems similar to FieldSearch but is "not allowed"

(defun categorized-field-search (search-terms &key (documentation nil))
  "Search for fields matching the provided term(s), for each relevant node in the category tree.
The keyword argument :documentation can be set to true (nil by default)."
;;Bug: there is no response at all when there is nothing to return, as a work-around the
;;     function checks that there will be a valid answer by calling field-search first.
  (when (field-search search-terms :documentation nil)
    (let* ((request (request "CategorizedFieldSearch" :search search-terms :return-documentation documentation))
	   (response #+lispworks (let ((mb (mp:make-mailbox)))
				   (setf (gethash (send-request request) *mailboxes*) mb)
				   (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
		     #-lispworks (progn (send-request request) (retrieve-response))))
      (loop for category in response
	 collect (if documentation
		     (unify:match ('((:|categoryName| ?category-name)
				     (:|categoryId| ?category-id)
				     (:|numFields| ?num-fields)
				     (:|description| ?description)
				     (:|isLeafNode| ?is-leaf-node)
				     ?fields)
				    category)
		       (make-category :name category-name :id category-id :description description
				      :num-fields num-fields :is-leaf-node is-leaf-node
				      :fields (loop for field in fields
						 collect (unify:match (#T(list (:|id| ?id)
									       #T(list (:|mnemonic| ?mnemonic)
										       (:|description| ?description)
										       #T(list :|datatype| &rest ?datatype)
										       (:|documentation| ?documentation)
										       (:|categoryName|)
										       (:|property|)
										       #T(list :|overrides| &rest ?overrides)
										       #T(list :|ftype| &rest ?ftype)))
									 field)
							   (make-field :id id
								       :mnemonic mnemonic
								       :description description
								       :overrides overrides
								       :documentation documentation
								       :datatype datatype
								       :ftype ftype)))))
		     (unify:match ('((:|categoryName| ?category-name)
				     (:|categoryId| ?category-id)
				     (:|numFields| ?num-fields)
				     (:|description| ?description)
				     (:|isLeafNode| ?is-leaf-node)
				     ?fields)
				    category)
		       (make-category :name category-name :id category-id :description description
				      :num-fields num-fields :is-leaf-node is-leaf-node
				      :fields (loop for field in fields
						 collect (unify:match (#T(list (:|id| ?id)
									       #T(list (:|mnemonic| ?mnemonic)
										       (:|description| ?description)
										       #T(list :|datatype| &rest ?datatype)
										       (:|categoryName|)
										       (:|property|)
										       #T(list :|overrides| &rest ?overrides)
										       #T(list :|ftype| &rest ?ftype)))
									 field)
							   (make-field :id id
								       :mnemonic mnemonic
								       :description description
								       :overrides overrides
								       :datatype datatype
								       :ftype ftype))))))))))
