(in-package :bloomie)

(defun beqs (name &key (group "GENERAL") (type :global))
  ;; alternative languages not supported (can create problems in decode-element)
  (assert (member type '(:private :global)))
  (let ((request (request "Beqs" :screen-name name :screen-type (symbol-name type) :group group :language-id nil)))
    #+lispworks (let ((mb (mp:make-mailbox)))
		  (setf (gethash (send-request request) *mailboxes*) mb)
		  (loop for (tree . done) = (mp:mailbox-read mb)
		     append (process-tree tree) until done))
    #-lispworks (progn (send-request request) (retrieve-response))))

(defun screen-data (name &key (folder "GENERAL") private)
  "Run a screen (see page EQS in the terminal).
Setting the private flag is in general not required for saved screens."
  (beqs name :group folder :type (if private :private :global)))

(defun screen-tickers (name &key (folder "GENERAL") private)
    "Run a screen (see page EQS in the terminal) and return the list of tickers.
Be aware that the whole set of output data defined in the screen will be retrieved. 
Setting the private flag is in general not required for saved screens."
  (mapcar #'first (screen-data name :folder folder :private private)))
