(in-package :bloomie)

(defvar *portfolio-fields* '((:members . "PORTFOLIO_MEMBERS")
			     (:positions . "PORTFOLIO_MPOSITION")
			     (:weights . "PORTFOLIO_MWEIGHT")
			     (:data . "PORTFOLIO_DATA")))

(defun bport (name fields &optional date)
  "Retrieves :members, :positions, :weights, or :data for the given portfolio (and optionally date)."
  (unless (member fields (mapcar #'first *portfolio-fields*))
    (error "fields should be one of~{ ~S~}" (mapcar #'first *portfolio-fields*)))
  (let ((request (request "PortfolioData" :securities name :fields (cdr (assoc fields *portfolio-fields*))
			  :overrides (if date `(("REFERENCE_DATE" ,(bbdate date)))))))
    #+lispworks (let ((mb (mp:make-mailbox)))
		  (setf (gethash (send-request request) *mailboxes*) mb)
		  (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
    #-lispworks (progn (send-request request) (retrieve-response))))

(defun portfolio-error (response)
  (let ((security-error (ignore-errors (unify:match ('(:|PortfolioDataResponse|
						       :|securityData|
						       (:|securityData|
							(:|security| ?security)
							(:|eidData|)
							(:|securityError| . ?security-error)
							(:|fieldExceptions|)
							(:|sequenceNumber| ?sequence-number)
							(:|fieldData|)))
						      response)
					 (cons security security-error)))))
    (when security-error
      (error 'security-error :security (car security-error)
	     :category (cadr (assoc :|category| (cdr security-error)))
	     :subcategory (cadr (assoc :|subcategory| (cdr security-error)))
	     :message (cadr (assoc :|message| (cdr security-error)))))))

(defun portfolio-empty (response)
  (ignore-errors
    (unify:match ('(:|PortfolioDataResponse|
		    :|securityData|
		    (:|securityData|
		     (:|security| ?id)
		     (:|eidData|)
		     (:|fieldExceptions|)
		     (:|sequenceNumber| 0)
		     (:|fieldData|)))
		   response)
      t)))

(defun portfolio-tickers (id &optional date)
  "Retrieves members for the given portfolio (v.g. U12345678-1, see portfolios in PRTU page) and optionally date."
  (let ((output (bport (concatenate 'string id " Client") :members date)))
    (or (portfolio-error output)
	(unless (portfolio-empty output)
	  (unify:match ('(:|PortfolioDataResponse|
			  :|securityData|
			  (:|securityData|
			   (:|security| ?id)
			   (:|eidData|)
			   (:|fieldExceptions|)
			   (:|sequenceNumber| 0)
			   (:|fieldData|
			    #T(list :PORTFOLIO_MEMBERS &rest ?members))))
			 output)
	    (loop for member in members
	       collect (unify:match ('(:PORTFOLIO_MEMBERS
				       (:|Security| ?security))
				      member)
			 security)))))))

(defun portfolio-weights (id &optional date)
  "Retrieves weights for the given portfolio (v.g. U12345678-1, see portfolios in PRTU page) and optionally date."
  (let ((output (bport (concatenate 'string id " Client") :weights date)))
    (or (portfolio-error output)
	(unless (portfolio-empty output)
	  (unify:match ('(:|PortfolioDataResponse|
			  :|securityData|
			  (:|securityData|
			   (:|security| ?id)
			   (:|eidData|)
			   (:|fieldExceptions|)
			   (:|sequenceNumber| 0)
			   (:|fieldData|		  
			    #T(list :PORTFOLIO_MWEIGHT &rest ?members))))
			 output)
	    (loop for member in members 
	       collect (unify:match ('(:PORTFOLIO_MWEIGHT
				       (:|Security| ?security)
				       (:|Weight| ?weight))
				      member)
			 (list security weight))))))))

(defun portfolio-positions (id &optional date)
  "Retrieves positions for the given portfolio (v.g. U12345678-1, see portfolios in PRTU page) and optionally date."
  (let ((output (bport (concatenate 'string id " Client") :positions date)))
    (or (portfolio-error output)
	(unless (portfolio-empty output)
	(unify:match ('(:|PortfolioDataResponse|
			:|securityData|
			(:|securityData|
			 (:|security| ?id)
			 (:|eidData|)
			 (:|fieldExceptions|)
			 (:|sequenceNumber| 0)
			 (:|fieldData|		  
			  #T(list :PORTFOLIO_MPOSITION &rest ?members))))
		       output)
		     (loop for member in members 
			collect (unify:match ('(:PORTFOLIO_MPOSITION
						(:|Security| ?security)
						(:|Position| ?position))
					       member)
					     (list security position))))))))

(defstruct (pf-position (:conc-name pos-))
  security position market-value cost cost-date cost-fx-rate)

(defun portfolio-data (id &optional date)
  "Retrieves data for the given portfolio (v.g. U12345678-1, see portfolios in PRTU page) and optionally date."
  (let ((output (bport (concatenate 'string id " Client") :data date)))
    (or (portfolio-error output)
	(unless (portfolio-empty output)
	(unify:match ('(:|PortfolioDataResponse|
			:|securityData|
			(:|securityData|
			 (:|security| ?id)
			 (:|eidData|)
			 (:|fieldExceptions|)
			 (:|sequenceNumber| 0)
			 (:|fieldData|		  
			  #T(list :PORTFOLIO_DATA &rest ?members))))
		       output)
		     (loop for member in members 
			collect (unify:match ('(:PORTFOLIO_DATA
						(:|Security| ?security)
						(:|Position| ?position)
						(:|Market Value| ?value)
						(:|Cost| ?cost)
						(:|Cost Date| ?date)
						(:|Cost Fx Rate| ?fxrate))
					       member)
					     (make-pf-position :security security :position position :market-value value
							       :cost cost :cost-fx-rate fxrate
							       :cost-date date))))))))
