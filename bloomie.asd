(asdf:defsystem #:bloomie
    :name "bloomie"
    :serial t
    :description "BLPAPI wrapper (http://www.bloomberglabs.com/api/)"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "MIT"
    :serial t
    :in-order-to ((test-op (test-op "bloomie-test")))
    :components ((:module #:ffi :pathname "ffi" :components ((:static-file "blpapi-3.11.1.1.h")))
		 (:file "package")
		 (:file "wrapper")
		 (:file "helper")
		 (:file "session")
		 (:file "values")
		 (:file "element")
		 (:file "response")
		 (:file "request")
		 ;; no CFFI / FLI especific code after this point
		 (:file "tree")
		 (:file "field-info")
		 (:file "instruments")
		 (:file "reference")
		 (:file "historical")
		 (:file "intraday")
		 (:file "screen")
		 (:file "portfolio")
		 (:file "indices")
		 (:file "earnings")
		 (:file "recommendations")
		 (:file "products"))
    :depends-on (:local-time :cl-unification #-lispworks :cl-autowrap #-lispworks :cl-plus-c))

(defsystem #:bloomie-test
    :name "bloomie test suite"
    :author "Carlos Ungil <ungil@mac.com>"
    :license "MIT"
    :depends-on (:bloomie :fiveam)
    :components ((:file "test"))
    :perform (asdf:test-op (o s) (declare (ignore o s)) (uiop:symbol-call :bloomie.test '#:run)))
