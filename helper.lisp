(in-package :bloomie)

(defvar *na-double* -2.4245362661989844d-14)

(defvar *na-int* -2147483648)

(defun null-pointer (&optional type)
  #+lispworks (fli:make-pointer :type type :address 0)
  #-lispworks (cffi:make-pointer 0))

;; the following around methods are to avoid warnings when unifying

(defmethod cl.ext.dacf.unification::occurs-in-p :around ((var symbol) (pat t) env)
  (declare (ignore env))
  (unless #+lispworks(fli:pointerp pat)
	  #-lispworks(cffi:pointerp pat)
	  (call-next-method)))

(defmethod cl.ext.dacf.unification::occurs-in-p :around ((var symbol) (pat t) env)
  (declare (ignore env))
  (unless (typep pat 'local-time:timestamp)
	  (call-next-method)))

(defun check-errors (code)
  (unless (zerop code)
    (error "~A" (bb.ffi::blpapi-get-last-error-description code))))

(defun name-string-and-destroy (blpapi-name)
  (prog1 (bb.ffi::blpapi-name-string blpapi-name)
    (bb.ffi::blpapi-name-destroy blpapi-name)))

;; (loop for s in (apropos-list "+BLPAPI-ERROR-" "BB.FFI") collect (cons (symbol-value s) (intern (subseq (string-trim "+" (symbol-name s)) (length "BLPAPI-ERROR-")) "KEYWORD")))

(defun ensure-list (x)
  (if (listp x) x (list x)))

(defun n-days-ago (n)
  (format nil "~{~2,'0D~^-~}" 
	  (reverse (subseq (multiple-value-list
			    (decode-universal-time (- (get-universal-time) (* n 24 60 60))))
			   3 6))))

(defun bbdate (date)
  (assert (stringp date))
  (if (= 8 (length date))
      date
      (remove #\- date)))

(defun ping ()
  (let ((request (request "Ping")))
    (progn (send-request request) (retrieve-response))))

(defvar *schema-stream* t)

#+lispworks
(fli:define-foreign-callable (stream-writer :result-type :int :calling-convention :cdecl)
    ((data :pointer) (length :int) (stream :pointer))
  (declare (ignore stream))
  (princ (fli:convert-from-foreign-string data :length length) *schema-stream*)
  (finish-output *schema-stream*)
  length)

#-lispworks
(cffi:defcallback stream-writer :int ((data :pointer) (length :int) (stream :pointer))
  (declare (ignore stream))
  (princ (cffi:foreign-string-to-lisp data :count length) *schema-stream*)
  length)
