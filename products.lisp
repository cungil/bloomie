(in-package :bloomie)

(defvar *products* (make-hash-table))

(defun product-list (ticker)
  "Retrieves the list of products defined for a company (keeping them in cache)."
  (let ((cached (gethash (intern ticker "KEYWORD") *products* :missing)))
    (if (eq cached :missing)
	(setf (gethash (intern ticker "KEYWORD") *products*)
	      (mapcar (lambda (x) (mapcar #'second x))
		      (rest (mapcar #'rest (cdadar (bdp (format nil "~A Equity" ticker)
							"IEST_BRAND_PRODUCT_LIST"))))))
	cached)))

(defun multi-company-products ()
  "Reports the products that appear multiple times in the cache (see product-list)."
  (let ((agg (make-hash-table)))
    (loop for ticker being the hash-keys of *products*
       do (loop for (name id) in (gethash ticker *products*)
	     do (push (list ticker name) (gethash id agg))))
    (loop for res being the hash-values of agg
       when (> (length res) 1)
       do (print (append (remove-duplicates (mapcar #'second res) :test #'string=)
			 (mapcar #'symbol-name (mapcar #'first res)))))))

(defun product-forecast (ticker product &key (year (nth-value 5 (decode-universal-time (get-universal-time))))
					  (source :bst) (stat :mean) (revision-interval "1M"))
  "Return the sales estimates for the given company and product. Source can be :BST, :BLI, :BPE or :ALL.
Stat can be one of :MEAN, :STDEV, :MEDIAN, :HIGH, :LOW, :NUMEST (or :ALL to retrive the six figures).
To get revisions, set stat to one of :NET-UP, :NET-DOWN, :NET-UNCHANGED, :UP, :DOWN, :UNCHANGED, :ADD, :DROP, :CONFIRMED 
or :REVISIONS to get them all. The revision-interval argument (for example 2D, 1W, 3M, 1Y) is set to 1M by default."
  (if (listp year)
      (loop for y in year append (list y (product-forecast ticker product :year y :stat stat :source source :revision-interval revision-interval)))
      (if (eq source :all)
	  (loop for source in '(:bst :bli :bpe)
	     append (list source (product-forecast ticker product :year year :stat stat :source source)))
	  (case stat
	    (:all (loop for stat in '(:mean :sd :median :low :high :nest)
		     append (list stat (product-forecast ticker product :year year :stat stat :source source :revision-interval revision-interval))))
	    (:revisions (loop for stat in '(:net-up :net-down :net-unchanged :up :down :unchanged :add :drop :confirmed)
			   append (list stat (product-forecast ticker product :year year :stat stat :source source :revision-interval revision-interval))))
	    (t (let ((id (format nil "~A" (second (assoc product (product-list ticker) :test #'equalp))))
		    (year (format nil "~AY" year))
		    (source (ecase source (:bst "BST") (:bli "BLI") (:bpe "BPE"))) ;; can't select single broker ?
		    (stat (ecase stat
			    (:mean "MEAN") (:sd "STDDEV") (:median "MEDIAN") (:high "HIGH") (:low "LOW") (:nest "NUMEST")
			    ;; the following are meaningul only when BEST_REVISION_INTERVAL_OVERRIDE is defined
			    ;; Override specifying a time period spanning a number of days, weeks, months, or years, 
			    ;; ending today.  The override value contains a positive integer and a single character for 
			    ;; the time unit ('D' for 'Day', 'W' for 'Week', 'M' for 'Month', and 'Y' for 'Year'); an 
			    ;; override of '2W' for example would denote the period spanning 2 weeks ending today.  The 
			    ;; specified time period is the basis for calculation of certain consensus statistics.  The 
			    ;; consensus statistic is specified by the BEst Consensus Statistic Override (BE995, BEST_CONSENSUS_STAT_OVERRIDE).
			    (:net-up "NET_UP") (:net-down "NET_DN") (:net-unchanged "NET_UNCHG")
			    (:up "NUM_UP") (:down "NUM_DN") (:unchanged "NUM_UNCHG")
			    (:add "NUM_ADD") (:drop "NUM_DROP") (:confirmed "NUM_CONF"))))
		(cadar (cdar (bdp (format nil "~A Equity" ticker) "IEST_PHARMACEUTICAL_PROD_SALES"
				  :overrides `(("IEST_BRAND_PRODUCT_OVERRIDE" ,id)
					       ("BEST_FPERIOD_OVERRIDE" ,year)
					       ("BEST_DATA_SOURCE_OVERRIDE" ,source)
					       ("BEST_CONSENSUS_STAT_OVERRIDE" ,stat)
					       ("BEST_REVISION_INTERVAL_OVERRIDE" ,(or revision-interval "0D"))))))))))))
