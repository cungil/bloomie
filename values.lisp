(in-package :bloomie)

#+lispworks
(defun value-as-element (element i)
  (multiple-value-bind (res val)
      (bb.ffi::blpapi-element-get-value-as-element element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-element (element i)
  (plus-c:c-with ((val :pointer))
    (check-errors (bb.ffi:blpapi-element-get-value-as-element element (val plus-c:&) i))
    val))

#+lispworks
(defun value-as-string (element i)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-string element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-string (element i)
  (plus-c:c-with ((val :pointer))
    (check-errors (bb.ffi:blpapi-element-get-value-as-string element (val plus-c:&) i))
    (cffi:foreign-string-to-lisp val)))

#+lispworks
(defun value-as-bool (element i)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-bool element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-bool (element i)
  (plus-c:c-with ((val :int))
    (check-errors (bb.ffi:blpapi-element-get-value-as-bool element (val plus-c:&) i))
    val))

#+lispworks
(defun value-as-int32 (element i)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-int32 element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-int32 (element i)
  (plus-c:c-with ((val :int))
    (check-errors (bb.ffi:blpapi-element-get-value-as-int32 element (val plus-c:&) i))
    val))

#+lispworks
(defun value-as-int64 (element i)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-int64 element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-int64 (element i)
  (plus-c:c-with ((val :int))
    (check-errors (bb.ffi:blpapi-element-get-value-as-int64 element (val plus-c:&) i))
    val))

#+lispworks
(defun value-as-float64 (element i)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-float64 element nil i)
    (check-errors res)
    val))

#-lispworks
(defun value-as-float64 (element i)
  (plus-c:c-with ((val :double))
    (check-errors (bb.ffi:blpapi-element-get-value-as-float64 element (val plus-c:&) i))
    val))

#+lispworks
(defun decode-date (datetime-tag)
  (assert (zerop (fli:foreign-slot-value datetime-tag 'bb.ffi::offset)))
  (local-time:encode-timestamp 
   (* 1000000 (fli:foreign-slot-value datetime-tag 'bb.ffi::milli-seconds))
   (fli:foreign-slot-value datetime-tag 'bb.ffi::seconds)
   (fli:foreign-slot-value datetime-tag 'bb.ffi::minutes)
   (fli:foreign-slot-value datetime-tag 'bb.ffi::hours)
   (fli:foreign-slot-value datetime-tag 'bb.ffi::day)
   (fli:foreign-slot-value datetime-tag 'bb.ffi::month)
   (fli:foreign-slot-value datetime-tag 'bb.ffi::year)
   :offset (fli:foreign-slot-value datetime-tag 'bb.ffi::offset)))

#-lispworks
(defun decode-date (datetime-tag)
  (assert (zerop (bb.ffi:blpapi-datetime-tag.offset datetime-tag)))
  (local-time:encode-timestamp 
   (* 1000000 (bb.ffi:blpapi-datetime-tag.milli-seconds datetime-tag))
   (bb.ffi:blpapi-datetime-tag.seconds datetime-tag)
   (bb.ffi:blpapi-datetime-tag.minutes datetime-tag)
   (bb.ffi:blpapi-datetime-tag.hours datetime-tag)
   (bb.ffi:blpapi-datetime-tag.day datetime-tag)
   (bb.ffi:blpapi-datetime-tag.month datetime-tag)
   (bb.ffi:blpapi-datetime-tag.year datetime-tag)
   :offset (bb.ffi:blpapi-datetime-tag.offset datetime-tag)))

(defvar *dates-as-iso* t
  "If nil, return the local-time timestamp; if true, return yyyy-mm-dd")

#+lispworks
(defun value-as-date (element i &optional keep-time-part)
  (multiple-value-bind (res val)
	(bb.ffi::blpapi-element-get-value-as-datetime element nil i)
    (check-errors res)
    (let ((date (decode-date val)))
      (if *dates-as-iso*
	  (local-time:format-rfc3339-timestring nil date :omit-time-part (not keep-time-part))
	  date))))
	  
#-lispworks
(defun value-as-date (element i &optional keep-time-part)
  (autowrap:with-alloc (date 'bb.ffi:blpapi-datetime-t)
    (check-errors (bb.ffi:blpapi-element-get-value-as-datetime element date i))
    (let ((date (decode-date date)))
      (if *dates-as-iso*
	  (local-time:format-rfc3339-timestring nil date :omit-time-part (not keep-time-part))
	  date))))
