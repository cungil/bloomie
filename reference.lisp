(in-package :bloomie)

(defun bdp (securities fields &key overrides)
  "Allows for multiple securities and fields. Overrides is an optional list with elements of the form (\"KEY\" \"VALUE\")."
  (let ((request (request "ReferenceData" :securities securities :fields fields :overrides overrides)))
    #+lispworks (let ((mb (mp:make-mailbox)))
		  (setf (gethash (send-request request) *mailboxes*) mb)
		  (loop for (tree . done) = (mp:mailbox-read mb) append (process-tree tree) until done))
    #-lispworks (progn (send-request request) (retrieve-response))))
