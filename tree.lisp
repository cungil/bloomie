(in-package :bloomie)

(defgeneric process-element (id rest))

(defun process-tree (tree)
  (case (length tree)
    (0 nil)
    (1 (process-tree (first tree)))
    (t (if (symbolp (first tree))
	   (process-element (car tree) (cdr tree))
	   (mapcar #'process-tree tree)))))

(defmethod process-element ((id symbol) rest)
  (cons id rest))

(defmethod process-element ((id (eql :|ReferenceDataResponse|)) rest)
  (assert (eq (first rest) :|securityData|))
  (mapcar #'process-tree (rest rest)))

(defmethod process-element ((id (eql :|HistoricalDataResponse|)) rest)
  (assert (eq (first rest) :|securityData|))
  (list (process-tree rest)))

(defmethod process-element ((id (eql :|IntradayBarResponse|)) rest)
  (ecase (first rest)
    (:|responseError| (error "~A" (second (assoc :|message| (rest rest)))))
    (:|barData| (process-tree rest))))

(defmethod process-element ((id (eql :|InstrumentListResponse|)) rest)
  (assert (and (= 1 (length rest)) (eq (first (first rest)) :|results|)))
  (mapcar #'process-tree (rest (first rest))))

(defmethod process-element ((id (eql :|GovtListResponse|)) rest)
  (assert (and (= 1 (length rest)) (eq (first (first rest)) :|results|)))
  (mapcar #'process-tree (rest (first rest))))

(defmethod process-element ((id (eql :|CurveListResponse|)) rest)
  (assert (and (= 1 (length rest)) (eq (first (first rest)) :|results|)))
  (mapcar #'process-tree (rest (first rest))))

(defmethod process-element ((id (eql :|results|)) rest)
  (mapcar #'second rest))

(defmethod process-element ((id (eql :|BeqsResponse|)) rest)
  (ecase (first rest)
    (:|responseError| (error "~A" (second (assoc :|message| (rest rest)))))
    (:|data|
      (assert (= 3 (length rest)))
      (assert (eq (first (second rest)) :|fieldDisplayUnits|))
      (assert (eq (first (third rest)) :|securityData|))
      (mapcar #'process-tree (rest (third rest))))))
	
(defmethod process-element ((id (eql :|fieldResponse|)) rest)
  (assert (eq (first rest) :|fieldData|))
  (mapcar #'process-tree (rest rest)))

(defmethod process-element ((id (eql :|categorizedFieldResponse|)) rest)
  (assert (eq (first rest) :|category|))
  (mapcar #'process-tree (rest rest)))

(define-condition security-error (error)
  ((security :initarg :security :reader security)
   (category :initarg :category :reader category)
   (subcategory :initarg :subcategory :reader subcategory)
   (message :initarg :message :reader message))
  (:report (lambda (c s)
             (format s "~A - ~A - ~A - ~A"
		     (security c) (category c) (subcategory c) (message c)))))

(defmethod print-object ((err security-error) stream)
  (print-unreadable-object (err stream :type t)
    (format stream "~A - ~A - ~A - ~A"
	    (security err) (category err) (subcategory err) (message err))))

(define-condition fields-error (error)
  ((security :initarg :security :reader security)
   (fields :initarg :fields :reader fields)
   (categories :initarg :categories :reader categories)
   (subcategories :initarg :subcategories :reader subcategories)
   (messages :initarg :messages :reader messages))
  (:report (lambda (c s)
             (format s "~A - ~A - ~A - ~A - ~A"
		     (security c) (fields c) (categories c) (subcategories c) (messages c)))))
 
(defmethod print-object ((err fields-error) stream)
  (print-unreadable-object (err stream :type t)
    (format stream "~A - ~A" (security err)
	    (loop for f in (fields err) for c in (categories err) for s in (subcategories err) for m in (messages err)
	       collect (list f c s m)))))

(defvar *raise-all-errors* nil
  "By default, security/field errors in BDP/BDH requests are handled to return the available data. 
Use this flag to disable this functionality and raise a continuable error each time.")

(defmethod process-element ((id (eql :|securityData|)) rest)
  ;; not valid assertions, can have any column in screens
  ;;(assert (equal '(:|eidData|) (assoc :|eidData| rest)))
  ;;(assert (integerp (second (assoc :|sequenceNumber| rest))))
  ;; (assert (null (set-exclusive-or (mapcar #'first rest) '(:|security| :|eidData| :|fieldExceptions| :|sequenceNumber| :|fieldData|))))
  ;; (cons (second (assoc :|security| rest)) (loop for (k v) in (rest (assoc :|fieldData| rest)) collect (cons k v)))
  (let* ((security (cadr (assoc :|security| rest)))
	 (output (if (equal (remove-duplicates (mapcar #'first rest)) '(:|securityData|))
		     (mapcar (lambda (x) (process-element (car x) (rest x))) rest)
		     (list security
			   (process-element (car (assoc :|fieldData| rest))
					    (cdr (assoc :|fieldData| rest)))))))
    (handler-case
	(progn
	  (let ((security-error (cdr (assoc :|securityError| rest))))
	    (when security-error
	      (let ((category (cadr (assoc :|category| security-error)))
		    (subcategory (cadr (assoc :|subcategory| security-error)))
		    (message (cadr (assoc :|message| security-error))))
		(error 'security-error
		       :security security :category category :subcategory subcategory :message message))))
	  (let ((field-exceptions (cdr (assoc :|fieldExceptions| rest))))
	    (when field-exceptions
	      (error 'fields-error :security security
		     :fields (loop for field-exception in (mapcar #'rest field-exceptions)
                                                               for field = (cadr (assoc :|fieldId| field-exception))
				collect field)
		     :categories (loop for field-exception in (mapcar #'rest field-exceptions)
				    for error-info = (cdr (assoc :|errorInfo| field-exception))
				    collect (cadr (assoc :|category| error-info)))
		     :subcategories (loop for field-exception in (mapcar #'rest field-exceptions)
				       for error-info = (cdr (assoc :|errorInfo| field-exception))
				       collect (cadr (assoc :|subcategory| error-info)))
		     :messages (loop for field-exception in (mapcar #'rest field-exceptions)
				  for error-info = (cdr (assoc :|errorInfo| field-exception))
				  collect (cadr (assoc :|message| error-info))))))
                  output)
      (security-error (c) (when *raise-all-errors* (cerror "continue" c))
		      (append output (list c)))
      (fields-error (c) (when *raise-all-errors* (cerror "continue" c))
		    (append output (list c))))))

(defstruct bar time open high low close volume num-events) ;; value)

(defmethod process-element ((id (eql :|barData|)) rest)
  (assert (equal '(:|eidData|) (assoc :|eidData| rest)))
  ;; do I want to signal when (:|delayedSecurity| 1) ?
  (loop for bar-tick-data in (rest (assoc :|barTickData| rest))
     collect (make-bar :time (second (assoc :|time| (rest bar-tick-data)))
		       :open (second (assoc :|open| (rest bar-tick-data)))
		       :high (second (assoc :|high| (rest bar-tick-data)))
		       :low (second (assoc :|low| (rest bar-tick-data)))
		       :close (second (assoc :|close| (rest bar-tick-data)))
		       :volume (second (assoc :|volume| (rest bar-tick-data)))
		       :num-events (second (assoc :|numEvents| (rest bar-tick-data)))
		       ;; what is this value element being returned?
		       ;; :value (second (assoc :|value| (rest bar-tick-data)))
		       )))
		       
(defmethod process-element ((id (eql :|fieldData|)) rest)
  (if (equal (remove-duplicates (mapcar #'first rest)) '(:|fieldData|))
      (mapcar (lambda (x) (process-element (car x) (cdr x))) rest)
      (process-tree rest)))

(defmethod process-element ((id (eql :|fieldDisplayUnits|)) rest)
  rest)

(defmethod process-element ((id (eql :|fieldInfo|)) rest)
  (mapcar (lambda (x) (process-element (car x) (cdr x))) rest))

(defmethod process-element ((id (eql :|category|)) rest)
  (mapcar (lambda (x) (process-element (car x) (cdr x))) rest))

(defmethod process-element ((id (eql :BLOOMBERG_PEERS)) rest)
  (list :BLOOMBERG_PEERS (mapcar #'cadadr rest)))

(defmethod process-element ((id (eql :CIE_DES_BULK)) rest)
  (list :CIE_DES_BULK (format nil "~{~A~^ ~}" (loop for (a (b c)) in rest collect c))))

(defmethod process-element ((id (eql :HIST_TRR_MONTHLY)) rest)
  (list :HIST_TRR_MONHTLY (reverse (mapcar (lambda (x) (let ((mm/dd/yyyy (cadadr x))
							     (ret (cdaddr x)))
							 (cons (format nil "~A-~A-~A" (subseq mm/dd/yyyy 6 10) (subseq mm/dd/yyyy 0 2) (subseq mm/dd/yyyy 3 5))
						       ret)))
					   rest))))

(defmethod process-element ((id (eql :EARN_ANN_DT_TIME_HIST_WITH_EPS)) rest)
  (assert (equal (mapcar #'first (rest (first rest)))
		 '(:|Year/Period| :|Announcement Date| :|Announcement Time|
		   :|Earnings EPS| :|Comparable EPS| :|Estimate EPS|)))
  (list :EARNING_ANN_DT_TIME_HIST_WITH_EPS
	(mapcar (lambda (x) (mapcar #'second (rest x))) rest)))

(defmethod process-element ((id (eql :ERN_ANN_DT_AND_PER)) rest)
  (assert (equal (mapcar #'first (rest (first rest)))
		 '(:|Earnings Announcement Date| :|Earnings Year and Period|)))
  (list :ERN_ANN_DT_AND_PER
	(mapcar (lambda (x) (mapcar #'second (rest x))) rest)))
