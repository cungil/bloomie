(in-package :bloomie.test)

(defun almost-equal (a b &optional (epsilon 1e3))
  (if (and (listp a) (listp b))
      (every #'identity (mapcar #'almost-equal a b))
      (cond ((and (numberp a) (numberp b))
	     (or (and (zerop a) (zerop b))
		 (and (not (zerop a)) (not (zerop b))
		      (< (/ (abs (- a b)) (+ a b)) epsilon))))
	    ((and (stringp a) (stringp b))
	     (string= a b))
	    (t (equal a b)))))

(defun run ()
  (when (bb::blpapi-library-loaded)
    #-ccl (cerror "Close library before running tests."
		  "The BLPAPI library should not be loaded when the test suite is launched.")
    #+ccl (error "The BLPAPI library should not be loaded when the test suite is launched.
ClozureCL does not support closing and reloading a library, please restart to run the tests.")
    (bb::close-blpapi-library))
  (5am:run 'bloomie-suite))

(5am:def-suite bloomie-suite)

(5am:in-suite bloomie-suite)

(5am:test library
  (5am:is (null (bb::blpapi-library-loaded)))
  (5am:is (not (null (bb::load-blpapi-library))))
  (5am:is (not (null (bb::blpapi-library-loaded))))
  #-ccl (5am:is (null (bb::load-blpapi-library)))
  #-ccl (5am:is (not (null (bb::load-blpapi-library :if-loaded :close-and-reload)))))

(5am:test (connection :depends-on library)
  (5am:is (eq (type-of (open-session))
	      #+lispworks 'fli::pointer
	      #-lispworks 'BLOOMIE.FFI:BLPAPI-SESSION-T))
  (5am:is (eq (type-of (progn (close-session) (open-session)))
	      #+lispworks 'fli::pointer
	      #-lispworks 'BLOOMIE.FFI:BLPAPI-SESSION-T)))

(5am:test (bdp :depends-on connection)
  (5am:is (equal (bdp "AAPL US Equity" "NAME")
		 '(("AAPL US Equity" (:NAME "APPLE INC")))))
  (5am:is (equal (bdp '("IBM US Equity" "MSFT US Equity") '("COUNTRY" "CRNCY"))
		 '(("IBM US Equity" ((:COUNTRY "US") (:CRNCY "USD")))
		   ("MSFT US Equity" ((:COUNTRY "US") (:CRNCY "USD"))))))
  (5am:is (= 2 (length (bb:bdp '("AAPL US Equity" "MSFT US Equity") '("XXXXXX" "YYYYYY")))))
  (5am:signals bb::fields-error (let ((bb:*raise-all-errors* t))
				  (bb:bdp '("AAPL US Equity" "MSFT US Equity") '("XXXXXX" "YYYYYY")))))

(5am:test (bdh :depends-on connection)
  (5am:is (almost-equal (bdh '("AAPL US Equity" "MSFT US Equity") '("PE_RATIO" "FREE_CASH_FLOW_YIELD")
			     :periodicity :monthly :start-date "2014-12-15" :end-date "2015-01-15")
			'(("AAPL US Equity"
			   (((:|date| "2014-12-31") (:PE_RATIO 14.876d0) (:FREE_CASH_FLOW_YIELD 9.154d0))))
			  ("MSFT US Equity"
			   (((:|date| "2014-12-31") (:PE_RATIO 17.7022d0) (:FREE_CASH_FLOW_YIELD 7.0428d0))))))))

(5am:test (instruments :depends-on connection)
  (5am:is (equal (instruments "General" :yellow-key :eqty :max-results 2)
		 '(("GE US<equity>" "General Electric Co (U.S.)")
		   ("GM US<equity>" "General Motors Co (U.S.)"))))
  (5am:is (= 10 (length (instruments "company" :max-results 10))))
  (5am:is (null (govt "rome" :ticker "ROM" :partial-match nil)))
  (5am:is (plusp (length (govt "rome" :ticker "ROM"))))
  (5am:is (= 3 (length (bb:curve "garch" :currency-code "CHF" :max-results 3)))))

(5am:test (field-info :depends-on connection)
  (5am:is (equal (sort (mapcar #'field-mnemonic (field-info '("BE998" "BE051") :documentation nil)) #'string<)
		 '("BEST_DATA_SOURCE_OVERRIDE" "BEST_PE_RATIO")))
  (5am:is (equal (sort (mapcar #'field-description (field-info '("PX_LAST" "LAST_PRICE"))) #'string<)
		 '("Last Price" "Last Trade/Last Price")))
  (5am:is (equal (mapcar #'field-documentation (field-info "PX_LAST" :documentation nil)) '(nil)))
  (5am:is (> (length (field-search "internal")) 200))
  (5am:is (> (length (field-list :type :real-time)) 1000)))

(5am:test (field-search :depends-on connection)
  (5am:is (plusp (length (bb:field-search "CEO age"))))
  (5am:is (member "ES169" (mapcar #'field-id (field-search "PRESID*")) :test #'string=))
  (5am:is (null (field-search "xxxxxxxxx")))
  (5am:is (equal 'bb::field (first (remove-duplicates (mapcar #'type-of (field-search "PRESID*"))))))
  (5am:is (equal 'bb::category (first (remove-duplicates (mapcar #'type-of (categorized-field-search "PRESID*"))))))
  (5am:is (null (categorized-field-search "xxxxxxxxx"))))

(5am:test (index-composition :depends-on connection)
  (5am:is (string= "AAPL US" (first (index-composition "INDU"))))
  (5am:is (almost-equal 100 (reduce #'+ (mapcar #'cdr (index-composition "INDU" :weights t)))))
  (5am:signals bb::security-error (index-composition "XYXYXYX")))

(5am:test (recommendations :depends-on connection)
  (5am:signals bb::security-error (bb:stock-recommendations "XYXYXYX"))
  (5am:signals bb::security-error (bb:stock-recommendations "XYXYXYX" "2016-12-31"))
  (5am:is (string= (reco-ticker (first (bb:stock-recommendations "BRK/A US"))) "BRK/A US"))
  (5am:is (string= (reco-as-of (first (bb:stock-recommendations "BRK/A US" "2016-12-31"))) "2016-12-31")))

(5am:test (earnings :depends-on connection)
  (5am:is (> (length (bb:get-earnings-dates "AAPL US")) 100))
  (5am:is (= 4 (length (bb:get-earnings "AAPL US" :start "2015-12-31" :end "2016-12-31"))))
  (5am:is (= (bb:eps-reported (first (bb:get-earnings "AAPL US" :start "2016-12-31" :end "2017-12-31")))
	     (bb:eps-reported (first (bb:get-earnings "AAPL US" :start "2016-12-31" :end "2017-12-31" :currency "USD")))))
  (5am:is (> (bb:eps-reported (first (bb:get-earnings "AAPL US" :start "2016-12-31" :end "2017-12-31")))
	     (bb:eps-reported (first (bb:get-earnings "AAPL US" :start "2016-12-31" :end "2017-12-31" :currency "EUR"))))))

(5am:test (products :depends-on connection)
  (5am:is (find "Orkambi" (bb:product-list "VRTX US") :test #'string= :key #'first))
  (5am:is (= 12 (length (bb:product-forecast "VRTX US" "Orkambi" :stat :all)))))

(5am:test (portfolio :depends-on connection)
  (5am:is (null (portfolio-tickers "U19703524-2")))
  (5am:is (almost-equal '("CELG US Equity" "ROG VX Equity") (portfolio-tickers "U19703524-2")))
  (5am:is (almost-equal '("CELG US Equity" "ROG VX Equity") (mapcar #'first (bb:portfolio-weights "U19703524-1"))))
  (5am:is (almost-equal '("CELG US Equity" "ROG VX Equity") (mapcar #'first (bb:portfolio-positions "U19703524-1"))))
  (5am:signals bb::security-error (portfolio-tickers "xxxx")))
