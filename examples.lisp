;; tested on Windows (SBCL 64-bit, ClozureCL 32/64-bit, Lispworks 6.0 32-bit)
;; the test suite should run without any errors: (asdf:test-system :bloomie)

(ql:quickload :bloomie)

(bb:open-session)

;; FIELDS

(bb:field-info "MILITARY_REVENUE")
(bb:field-info "M1090")
(bb:field-info '("px_last" "pe_ratio") :documentation nil)

(mapcar #'bb:field-mnemonic (bb:field-search "military"))

(loop for cat in (bb:categorized-field-search "PRESID*")
   do (format t "~&~A :~{ ~A~}" (bb:category-name cat)
	      (loop for fld in (bb:category-fields cat) collect (bb:field-mnemonic fld))))

(length (bb:field-list :type :real-time))
(length (bb:field-list :type :static)) ;; 57k+ fields

;; INSTRUMENTS

(bb:instruments "entropy")
(bb:instruments "entropy" :max-results 5)
(bb:instruments "entropy" :yellow-key :corp)
(bb:instruments "entropy" :yellow-key :indx)

(bb:govt "rome")
(bb:govt "rome" :max-results 1)
(bb:govt "rome" :ticker "ROM")
(bb:govt "rome" :ticker "ROM" :partial-match nil)
(bb:govt "rome" :ticker "ROMCTY" :partial-match nil)

(bb:curve "genev")
(bb:curve "" :curve-id "bi758")
(bb:curve "garch" :currency-code "CHF" :max-results 3)
(bb:curve "" :currency-code :chf :type :irs)
(mapcar #'curve-currency (bb:curve nil :currency-code "jpy" :subtype "spread"))

;; REFERENCE

(bb:bdp '("AAPL US Equity" "MSFT US Equity") '("PX_LAST" "BEST_EPS"))
(bb:bdp '("AAPL US Equity" "MSFT US Equity") '("XXXXXX" "YYYYYY" "PX_LAST"))
(bb:bdp '("AAAAPLE US Equity" "MSFT US Equity") '("XXXXXX" "YYYYYY" "PX_LAST"))
(let ((bb:*raise-all-errors* t))
  (bb:bdp '("AAAAPL US Equity" "MSFT US Equity") '("XXXXXX" "YYYYYY" "PX_LAST")))

;; HISTORICAL

(bb:bdh '("AAPL US Equity" "MSFT US Equity") '("PX_LAST" "VOLUME")
	:start-date "20170501" :end-date "20170510" :periodicity :daily)
(bb:bdh "AAPL US Equity" "PE_RATIO" :start-date "1995-12-31" :periodicity :yearly)

;; INTRADAY BARS

(bb:ibar "AAPL US Equity" "BID" :bar-size '(4 :hour)
	 :start-date-time "2017-08-08" :end-date-time "2017-08-11")
(bb:ibar "AAPL US Equity" "TRADE" :bar-size '(1 :m)
	 :start-date-time "2017-08-08T14" :end-date-time "2017-08-08T14:05")

;; PORTFOLIO

(bb:portfolio-tickers "U12345678-1")
(bb:portfolio-tickers "U12345678-1" "2017-08-04")
(bb:portfolio-weights "U12345678-1")
(bb:portfolio-positions "U12345678-1")
(bb:portfolio-data "U12345678-1")
(bb:portfolio-data "XXX")

;; EQUITY SCREENING

(bb:screen-tickers "Global Automotive Parts Retailers")
(bb:screen-tickers "Global Automotive Parts Retailers" :folder "General")
(bb:screen-tickers "Global Automotive Parts Retailers" :folder "Popular")

;; EARNINGS

(bb:get-earnings-dates "AAPL US")
(bb:get-earnings "AAPL US" :start "2016-12-31")
(bb:get-earnings "AAPL US" :start "2016-12-31" :currency "EUR")

;; PRODUCTS

(loop for ticker in (bb:index-composition "SPX") for prods = (bb:product-list ticker)
   when prods do (format t "~A~10T~A~&" ticker (length prods)))
(bb:product-list "VRTX US")
(bb:product-forecast "VRTX US" "Orkambi")
(bb:product-forecast "VRTX US" "Orkambi" :year '(2017 2018 2019 2020))
(bb:product-forecast "VRTX US" "Orkambi" :year 2020 :source :all)
(bb:product-forecast "VRTX US" "Orkambi" :stat :all)
(bb:product-forecast "VRTX US" "Orkambi" :stat :revisions)

;; EXPORT SCHEMAS AND FIELD DEFINITIONS

(bb:document-schemas) ;; can be incomplete, see function docstring

(bb:document-fields) ;; can be incomplete, see function docstring

(bb:close-session)
