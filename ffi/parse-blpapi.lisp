(require "foreign-parser")

;; tested with LW 6 only, works with Visual Studio Express 2012/2013
(foreign-parser:process-foreign-file
 (merge-pathnames "blpapi-3.11.1.1.h" *load-truename*)
 :dff (merge-pathnames "blpapi-dff.raw" *load-truename*)
 :case-sensitive :split-name
 :package (or (find-package "BLOOMIE.FFI") (make-package "BLOOMIE.FFI"))
 :preprocessor "C:/Program Files (x86)/Microsoft Visual Studio 11.0/VC/bin/cl.exe"
 :preprocessor-format-string "\"~A\" /nologo /D \"_MSC_VER=1100\" /E ~A ~{/D~A ~}~{/I\"~A\" ~}/Tc \"~A\""
 :include-path (list (merge-pathnames "blpapi-3.11.1.1" *load-truename*)
		     "C:/Program Files (x86)/Microsoft Visual Studio 11.0/VC/include"))
