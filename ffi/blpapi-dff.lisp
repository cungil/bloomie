#| DATE           : 5 Aug 2017 
 | USER           : cun 
 | PROCESSED FILE : C:\Users\cun\quicklisp\local-projects\bloomie\ffi\blpapi-3.11.1.1.h
 |#

(in-package "BLOOMIE.FFI")

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_types.h"

(fli:define-c-typedef (blpapi-bool-t (:foreign-name "blpapi_Bool_t"))
                      :int)
(fli:define-c-typedef (blpapi-char-t (:foreign-name "blpapi_Char_t"))
                      :char)
(fli:define-c-typedef (blpapi-uchar-t (:foreign-name "blpapi_UChar_t"))
                      (:unsigned :char))
(fli:define-c-typedef (blpapi-int16-t (:foreign-name "blpapi_Int16_t"))
                      :short)
(fli:define-c-typedef (blpapi-uint16-t
                       (:foreign-name "blpapi_UInt16_t"))
                      (:unsigned :short))
(fli:define-c-typedef (blpapi-int32-t (:foreign-name "blpapi_Int32_t"))
                      :int)
(fli:define-c-typedef (blpapi-uint32-t
                       (:foreign-name "blpapi_UInt32_t"))
                      (:unsigned :int))
(fli:define-c-typedef (blpapi-int64-t (:foreign-name "blpapi_Int64_t"))
                      :long-long)
(fli:define-c-typedef (blpapi-uint64-t
                       (:foreign-name "blpapi_UInt64_t"))
                      (:unsigned :long-long))
(fli:define-c-typedef (blpapi-float32-t
                       (:foreign-name "blpapi_Float32_t"))
                      :float)
(fli:define-c-typedef (blpapi-float64-t
                       (:foreign-name "blpapi_Float64_t"))
                      :double)
(fli:define-c-enum (blpapi-data-type-t
                    (:foreign-name "blpapi_DataType_t"))
                   (blpapi-datatype-bool 1)
                   (blpapi-datatype-char 2)
                   (blpapi-datatype-byte 3)
                   (blpapi-datatype-int32 4)
                   (blpapi-datatype-int64 5)
                   (blpapi-datatype-float32 6)
                   (blpapi-datatype-float64 7)
                   (blpapi-datatype-string 8)
                   (blpapi-datatype-bytearray 9)
                   (blpapi-datatype-date 10)
                   (blpapi-datatype-time 11)
                   (blpapi-datatype-decimal 12)
                   (blpapi-datatype-datetime 13)
                   (blpapi-datatype-enumeration 14)
                   (blpapi-datatype-sequence 15)
                   (blpapi-datatype-choice 16)
                   (blpapi-datatype-correlation-id 17))
(fli:define-c-enum (blpapi-logging-severity-t
                    (:foreign-name "blpapi_Logging_Severity_t"))
                   (blpapi-logging-severity-off 0)
                   (blpapi-logging-severity-fatal 1)
                   (blpapi-logging-severity-error 2)
                   (blpapi-logging-severity-warn 3)
                   (blpapi-logging-severity-info 4)
                   (blpapi-logging-severity-debug 5)
                   (blpapi-logging-severity-trace 6))
(fli:define-c-typedef (blpapi-logging-severity-t
                       (:foreign-name "blpapi_Logging_Severity_t"))
                      (:enum blpapi-logging-severity-t))
(fli:define-c-struct (blpapi-abstract-session
                      (:foreign-name "blpapi_AbstractSession")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-abstract-session-t
                       (:foreign-name "blpapi_AbstractSession_t"))
                      (:struct blpapi-abstract-session))
(fli:define-c-struct (blpapi-constant
                      (:foreign-name "blpapi_Constant")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-constant-t
                       (:foreign-name "blpapi_Constant_t"))
                      (:struct blpapi-constant))
(fli:define-c-struct (blpapi-constant-list
                      (:foreign-name "blpapi_ConstantList")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-constant-list-t
                       (:foreign-name "blpapi_ConstantList_t"))
                      (:struct blpapi-constant-list))
(fli:define-c-struct (blpapi-element
                      (:foreign-name "blpapi_Element")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-element-t
                       (:foreign-name "blpapi_Element_t"))
                      (:struct blpapi-element))
(fli:define-c-struct (blpapi-event
                      (:foreign-name "blpapi_Event")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-event-t (:foreign-name "blpapi_Event_t"))
                      (:struct blpapi-event))
(fli:define-c-struct (blpapi-event-dispatcher
                      (:foreign-name "blpapi_EventDispatcher")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-event-dispatcher-t
                       (:foreign-name "blpapi_EventDispatcher_t"))
                      (:struct blpapi-event-dispatcher))
(fli:define-c-struct (blpapi-event-formatter
                      (:foreign-name "blpapi_EventFormatter")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-event-formatter-t
                       (:foreign-name "blpapi_EventFormatter_t"))
                      (:struct blpapi-event-formatter))
(fli:define-c-struct (blpapi-event-queue
                      (:foreign-name "blpapi_EventQueue")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-event-queue-t
                       (:foreign-name "blpapi_EventQueue_t"))
                      (:struct blpapi-event-queue))
(fli:define-c-struct (blpapi-message-iterator
                      (:foreign-name "blpapi_MessageIterator")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-message-iterator-t
                       (:foreign-name "blpapi_MessageIterator_t"))
                      (:struct blpapi-message-iterator))
(fli:define-c-struct (blpapi-name
                      (:foreign-name "blpapi_Name")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-name-t (:foreign-name "blpapi_Name_t"))
                      (:struct blpapi-name))
(fli:define-c-struct (blpapi-operation
                      (:foreign-name "blpapi_Operation")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-operation-t
                       (:foreign-name "blpapi_Operation_t"))
                      (:struct blpapi-operation))
(fli:define-c-struct (blpapi-provider-session
                      (:foreign-name "blpapi_ProviderSession")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-provider-session-t
                       (:foreign-name "blpapi_ProviderSession_t"))
                      (:struct blpapi-provider-session))
(fli:define-c-struct (blpapi-request-template
                      (:foreign-name "blpapi_RequestTemplate")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-request-template-t
                       (:foreign-name "blpapi_RequestTemplate_t"))
                      (:struct blpapi-request-template))
(fli:define-c-struct (blpapi-service
                      (:foreign-name "blpapi_Service")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-service-t
                       (:foreign-name "blpapi_Service_t"))
                      (:struct blpapi-service))
(fli:define-c-struct (blpapi-session
                      (:foreign-name "blpapi_Session")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-session-t
                       (:foreign-name "blpapi_Session_t"))
                      (:struct blpapi-session))
(fli:define-c-struct (blpapi-session-options
                      (:foreign-name "blpapi_SessionOptions")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-session-options-t
                       (:foreign-name "blpapi_SessionOptions_t"))
                      (:struct blpapi-session-options))
(fli:define-c-struct (blpapi-tls-options
                      (:foreign-name "blpapi_TlsOptions")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-tls-options-t
                       (:foreign-name "blpapi_TlsOptions_t"))
                      (:struct blpapi-tls-options))
(fli:define-c-struct (blpapi-subscription-itrerator
                      (:foreign-name "blpapi_SubscriptionItrerator")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-subscription-iterator-t
                       (:foreign-name "blpapi_SubscriptionIterator_t"))
                      (:struct blpapi-subscription-itrerator))
(fli:define-c-struct (blpapi-identity
                      (:foreign-name "blpapi_Identity")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-user-handle
                       (:foreign-name "blpapi_UserHandle"))
                      (:struct blpapi-identity))
(fli:define-c-typedef (blpapi-user-handle-t
                       (:foreign-name "blpapi_UserHandle_t"))
                      (:struct blpapi-identity))
(fli:define-c-struct (blpapi-identity
                      (:foreign-name "blpapi_Identity")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-identity-t
                       (:foreign-name "blpapi_Identity_t"))
                      (:struct blpapi-identity))

;;; Derived from file : "C:\\Program Files (x86)\\Microsoft Visual Studio 11.0\\VC\\include\\vadefs.h"

(fli:define-c-typedef (uintptr-t (:foreign-name "uintptr_t"))
                      (:unsigned :int))
(fli:define-c-typedef (va-list (:foreign-name "va_list"))
                      (:pointer :char))

;;; Derived from file : "C:\\Program Files (x86)\\Microsoft Visual Studio 11.0\\VC\\include\\crtdefs.h"

(fli:define-c-typedef (size-t (:foreign-name "size_t"))
                      (:unsigned :int))
(fli:define-c-typedef (rsize-t (:foreign-name "rsize_t")) size-t)
(fli:define-c-typedef (intptr-t (:foreign-name "intptr_t")) :int)
(fli:define-c-typedef (ptrdiff-t (:foreign-name "ptrdiff_t")) :int)
(fli:define-c-typedef (wchar-t (:foreign-name "wchar_t"))
                      (:unsigned :short))
(fli:define-c-typedef (wint-t (:foreign-name "wint_t"))
                      (:unsigned :short))
(fli:define-c-typedef (wctype-t (:foreign-name "wctype_t"))
                      (:unsigned :short))
(fli:define-c-typedef (errno-t (:foreign-name "errno_t")) :int)
(fli:define-c-typedef (--time32-t (:foreign-name "__time32_t")) :long)
(fli:define-c-typedef (--time64-t (:foreign-name "__time64_t"))
                      :long-long)
(fli:define-c-typedef (time-t (:foreign-name "time_t")) --time64-t)
(fli:define-foreign-function (-invalid-parameter-noinfo
                              "_invalid_parameter_noinfo"
                              :source)
                             nil
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (-invalid-parameter-noinfo-noreturn
                              "_invalid_parameter_noinfo_noreturn"
                              :source)
                             nil
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (-invoke-watson "_invoke_watson" :source)
                             ((arg-1 (:pointer (:const wchar-t)))
                              (arg-2 (:pointer (:const wchar-t)))
                              (arg-3 (:pointer (:const wchar-t)))
                              (arg-4 (:unsigned :int))
                              (arg-5 uintptr-t))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-c-struct (threadlocaleinfostruct
                      (:foreign-name "threadlocaleinfostruct")
                      (:forward-reference t)))
(fli:define-c-struct (threadmbcinfostruct
                      (:foreign-name "threadmbcinfostruct")
                      (:forward-reference t)))
(fli:define-c-typedef (pthreadlocinfo (:foreign-name "pthreadlocinfo"))
                      (:pointer (:struct threadlocaleinfostruct)))
(fli:define-c-typedef (pthreadmbcinfo (:foreign-name "pthreadmbcinfo"))
                      (:pointer (:struct threadmbcinfostruct)))
(fli:define-c-struct (--lc-time-data
                      (:foreign-name "__lc_time_data")
                      (:forward-reference t)))
(fli:define-c-struct (localeinfo-struct
                      (:foreign-name "localeinfo_struct"))
                     (locinfo pthreadlocinfo)
                     (mbcinfo pthreadmbcinfo))
(fli:define-c-typedef (-locale-tstruct
                       (:foreign-name "_locale_tstruct"))
                      (:struct localeinfo-struct))
(fli:define-c-typedef (-locale-t (:foreign-name "_locale_t"))
                      (:pointer (:struct localeinfo-struct)))
(fli:define-c-struct (localerefcount (:foreign-name "localerefcount"))
                     (locale (:pointer :char))
                     (wlocale (:pointer wchar-t))
                     (refcount (:pointer :int))
                     (wrefcount (:pointer :int)))
(fli:define-c-typedef (locrefcount (:foreign-name "locrefcount"))
                      (:struct localerefcount))
(fli:define-c-struct (lconv
                      (:foreign-name "lconv")
                      (:forward-reference t)))
(fli:define-c-struct (threadlocaleinfostruct
                      (:foreign-name "threadlocaleinfostruct"))
                     (refcount :int)
                     (lc-codepage (:unsigned :int))
                     (lc-collate-cp (:unsigned :int))
                     (lc-time-cp (:unsigned :int))
                     (lc-category (:c-array locrefcount 6))
                     (lc-clike :int)
                     (mb-cur-max :int)
                     (lconv-intl-refcount (:pointer :int))
                     (lconv-num-refcount (:pointer :int))
                     (lconv-mon-refcount (:pointer :int))
                     (lconv (:pointer (:struct lconv)))
                     (ctype1-refcount (:pointer :int))
                     (ctype1 (:pointer (:unsigned :short)))
                     (pctype (:pointer (:const (:unsigned :short))))
                     (pclmap (:pointer (:const (:unsigned :char))))
                     (pcumap (:pointer (:const (:unsigned :char))))
                     (lc-time-curr (:pointer (:struct --lc-time-data)))
                     (locale-name (:c-array (:pointer wchar-t) 6)))
(fli:define-c-typedef (threadlocinfo (:foreign-name "threadlocinfo"))
                      (:struct threadlocaleinfostruct))

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_correlationid.h"

(fli:define-c-struct (blpapi-managed-ptr-t-
                      (:foreign-name "blpapi_ManagedPtr_t_")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-managed-ptr-t
                       (:foreign-name "blpapi_ManagedPtr_t"))
                      (:struct blpapi-managed-ptr-t-))
(fli:define-c-typedef (blpapi-managed-ptr-manager-function-t
                       (:foreign-name
                        "blpapi_ManagedPtr_ManagerFunction_t"))
                      (:pointer
                       (:function
                        ((:pointer blpapi-managed-ptr-t)
                         (:pointer (:const blpapi-managed-ptr-t))
                         :int)
                        :int
                        :calling-convention
                        :cdecl)))
(fli:define-c-typedef (blpapi-managed-ptr-t-data-
                       (:foreign-name "blpapi_ManagedPtr_t_data_"))
                      (:union (int-value :int) (ptr (:pointer :void))))
(fli:define-c-struct (blpapi-managed-ptr-t-
                      (:foreign-name "blpapi_ManagedPtr_t_"))
                     (pointer (:pointer :void))
                     (user-data
                      (:c-array blpapi-managed-ptr-t-data- 4))
                     (manager blpapi-managed-ptr-manager-function-t))
(fli:define-c-struct (blpapi-correlation-id-t-
                      (:foreign-name "blpapi_CorrelationId_t_"))
                     (size (:bit-field (:unsigned :int) 8))
                     (value-type (:bit-field (:unsigned :int) 4))
                     (class-id (:bit-field (:unsigned :int) 16))
                     (reserved (:bit-field (:unsigned :int) 4))
                     (value
                      (:union
                       (int-value blpapi-uint64-t)
                       (ptr-value blpapi-managed-ptr-t))))
(fli:define-c-typedef (blpapi-correlation-id-t
                       (:foreign-name "blpapi_CorrelationId_t"))
                      (:struct blpapi-correlation-id-t-))

;;; Derived from file : "C:\\Program Files (x86)\\Microsoft Visual Studio 11.0\\VC\\include\\stddef.h"

(fli:define-foreign-function (-errno "_errno" :source)
                             nil
                             :result-type
                             (:pointer :int)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (-set-errno "_set_errno" :source)
                             ((-value :int))
                             :result-type
                             errno-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (-get-errno "_get_errno" :source)
                             ((-value (:pointer :int)))
                             :result-type
                             errno-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (--threadid "__threadid" :source)
                             nil
                             :result-type
                             (:unsigned :long)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (--threadhandle "__threadhandle" :source)
                             nil
                             :result-type
                             uintptr-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_name.h"

(fli:define-foreign-function (blpapi-name-create
                              "blpapi_Name_create"
                              :source)
                             ((name-string (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-destroy
                              "blpapi_Name_destroy"
                              :source)
                             ((name (:pointer blpapi-name-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-duplicate
                              "blpapi_Name_duplicate"
                              :source)
                             ((src (:pointer (:const blpapi-name-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-equals-str
                              "blpapi_Name_equalsStr"
                              :source)
                             ((name (:pointer (:const blpapi-name-t)))
                              (string (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-string
                              "blpapi_Name_string"
                              :source)
                             ((name (:pointer (:const blpapi-name-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-length
                              "blpapi_Name_length"
                              :source)
                             ((name (:pointer (:const blpapi-name-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-name-find-name
                              "blpapi_Name_findName"
                              :source)
                             ((name-string (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_error.h"

(fli:define-foreign-function (blpapi-get-last-error-description
                              "blpapi_getLastErrorDescription"
                              :source)
                             ((result-code :int))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_exception.h"

(fli:define-c-struct (blpapi-error-info
                      (:foreign-name "blpapi_ErrorInfo"))
                     (exception-class :int)
                     (description (:c-array :char 256)))
(fli:define-c-typedef (blpapi-error-info-t
                       (:foreign-name "blpapi_ErrorInfo_t"))
                      (:struct blpapi-error-info))
(fli:define-foreign-function (blpapi-get-error-info
                              "blpapi_getErrorInfo"
                              :source)
                             ((buffer (:reference-return blpapi-error-info-t))
                              (error-code :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_streamproxy.h"

(fli:define-c-typedef (blpapi-stream-writer-t
                       (:foreign-name "blpapi_StreamWriter_t"))
                      (:pointer
                       (:function
                        ((:reference :ef-mb-string :allow-null t)
                         :int
                         (:pointer :void))
                        :int
                        :calling-convention
                        :cdecl)))

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_timepoint.h"

(fli:define-c-struct (blpapi-time-point
                      (:foreign-name "blpapi_TimePoint"))
                     (d-value blpapi-int64-t))
(fli:define-c-typedef (blpapi-time-point-t
                       (:foreign-name "blpapi_TimePoint_t"))
                      (:struct blpapi-time-point))
(fli:define-foreign-function (blpapi-time-point-util-nanoseconds-between
                              "blpapi_TimePointUtil_nanosecondsBetween"
                              :source)
                             ((start
                               (:pointer (:const blpapi-time-point-t)))
                              (end
                               (:pointer
                                (:const blpapi-time-point-t))))
                             :result-type
                             :long-long
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_datetime.h"

(fli:define-c-struct (blpapi-datetime-tag
                      (:foreign-name "blpapi_Datetime_tag"))
                     (parts blpapi-uchar-t)
                     (hours blpapi-uchar-t)
                     (minutes blpapi-uchar-t)
                     (seconds blpapi-uchar-t)
                     (milli-seconds blpapi-uint16-t)
                     (month blpapi-uchar-t)
                     (day blpapi-uchar-t)
                     (year blpapi-uint16-t)
                     (offset blpapi-int16-t))
(fli:define-c-typedef (blpapi-datetime-t
                       (:foreign-name "blpapi_Datetime_t"))
                      (:struct blpapi-datetime-tag))
(fli:define-c-struct (blpapi-high-precision-datetime-tag
                      (:foreign-name
                       "blpapi_HighPrecisionDatetime_tag"))
                     (datetime blpapi-datetime-t)
                     (picoseconds blpapi-uint32-t))
(fli:define-c-typedef (blpapi-high-precision-datetime-t
                       (:foreign-name
                        "blpapi_HighPrecisionDatetime_t"))
                      (:struct blpapi-high-precision-datetime-tag))
(fli:define-foreign-function (blpapi-datetime-compare
                              "blpapi_Datetime_compare"
                              :source)
                             ((lhs blpapi-datetime-t)
                              (rhs blpapi-datetime-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-datetime-print
                              "blpapi_Datetime_print"
                              :source)
                             ((datetime
                               (:pointer (:const blpapi-datetime-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-high-precision-datetime-compare
                              "blpapi_HighPrecisionDatetime_compare"
                              :source)
                             ((lhs
                               (:pointer
                                (:const
                                 blpapi-high-precision-datetime-t)))
                              (rhs
                               (:pointer
                                (:const
                                 blpapi-high-precision-datetime-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-high-precision-datetime-print
                              "blpapi_HighPrecisionDatetime_print"
                              :source)
                             ((datetime
                               (:pointer
                                (:const
                                 blpapi-high-precision-datetime-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-high-precision-datetime-from-time-point
                              "blpapi_HighPrecisionDatetime_fromTimePoint"
                              :source)
                             ((datetime
                               (:pointer
                                blpapi-high-precision-datetime-t))
                              (time-point
                               (:pointer (:const blpapi-time-point-t)))
                              (offset :short))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_constant.h"

(fli:define-foreign-function (blpapi-constant-set-user-data
                              "blpapi_Constant_setUserData"
                              :source)
                             ((constant (:pointer blpapi-constant-t))
                              (userdata (:pointer :void)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-name
                              "blpapi_Constant_name"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-description
                              "blpapi_Constant_description"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-status
                              "blpapi_Constant_status"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-datatype
                              "blpapi_Constant_datatype"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-char
                              "blpapi_Constant_getValueAsChar"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-char-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-int32
                              "blpapi_Constant_getValueAsInt32"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-int32-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-int64
                              "blpapi_Constant_getValueAsInt64"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-int64-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-float32
                              "blpapi_Constant_getValueAsFloat32"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-float32-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-float64
                              "blpapi_Constant_getValueAsFloat64"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-float64-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-datetime
                              "blpapi_Constant_getValueAsDatetime"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer (:reference-return blpapi-datetime-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-get-value-as-string
                              "blpapi_Constant_getValueAsString"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t)))
                              (buffer
                               (:reference-return (:reference :ef-mb-string :allow-null t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-user-data
                              "blpapi_Constant_userData"
                              :source)
                             ((constant
                               (:pointer (:const blpapi-constant-t))))
                             :result-type
                             (:pointer :void)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-set-user-data
                              "blpapi_ConstantList_setUserData"
                              :source)
                             ((constant
                               (:pointer blpapi-constant-list-t))
                              (userdata (:pointer :void)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-name
                              "blpapi_ConstantList_name"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-constant-list-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-description
                              "blpapi_ConstantList_description"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-constant-list-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-num-constants
                              "blpapi_ConstantList_numConstants"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-constant-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-datatype
                              "blpapi_ConstantList_datatype"
                              :source)
                             ((constant
                               (:pointer
                                (:const blpapi-constant-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-status
                              "blpapi_ConstantList_status"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-constant-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-get-constant
                              "blpapi_ConstantList_getConstant"
                              :source)
                             ((constant
                               (:pointer
                                (:const blpapi-constant-list-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             (:pointer blpapi-constant-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-get-constant-at
                              "blpapi_ConstantList_getConstantAt"
                              :source)
                             ((constant
                               (:pointer
                                (:const blpapi-constant-list-t)))
                              (index size-t))
                             :result-type
                             (:pointer blpapi-constant-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-constant-list-user-data
                              "blpapi_ConstantList_userData"
                              :source)
                             ((constant
                               (:pointer
                                (:const blpapi-constant-list-t))))
                             :result-type
                             (:pointer :void)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_schema.h"

(fli:define-c-typedef (blpapi-schema-element-definition-t
                       (:foreign-name
                        "blpapi_SchemaElementDefinition_t"))
                      (:pointer :void))
(fli:define-c-typedef (blpapi-schema-type-definition-t
                       (:foreign-name "blpapi_SchemaTypeDefinition_t"))
                      (:pointer :void))
(fli:define-foreign-function (blpapi-schema-element-definition-name
                              "blpapi_SchemaElementDefinition_name"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-description
                              "blpapi_SchemaElementDefinition_description"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-status
                              "blpapi_SchemaElementDefinition_status"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-type
                              "blpapi_SchemaElementDefinition_type"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             (:pointer blpapi-schema-type-definition-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-num-alternate-names
                              "blpapi_SchemaElementDefinition_numAlternateNames"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-get-alternate-name
                              "blpapi_SchemaElementDefinition_getAlternateName"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t)))
                              (index size-t))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-min-values
                              "blpapi_SchemaElementDefinition_minValues"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-max-values
                              "blpapi_SchemaElementDefinition_maxValues"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-print
                              "blpapi_SchemaElementDefinition_print"
                              :source)
                             ((element
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (user-stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-set-user-data
                              "blpapi_SchemaElementDefinition_setUserData"
                              :source)
                             ((field
                               (:pointer
                                blpapi-schema-element-definition-t))
                              (userdata (:pointer :void)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-element-definition-user-data
                              "blpapi_SchemaElementDefinition_userData"
                              :source)
                             ((field
                               (:pointer
                                (:const
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             (:pointer :void)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-name
                              "blpapi_SchemaTypeDefinition_name"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-description
                              "blpapi_SchemaTypeDefinition_description"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-status
                              "blpapi_SchemaTypeDefinition_status"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-datatype
                              "blpapi_SchemaTypeDefinition_datatype"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-complex-type
                              "blpapi_SchemaTypeDefinition_isComplexType"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-simple-type
                              "blpapi_SchemaTypeDefinition_isSimpleType"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-enumeration-type
                              "blpapi_SchemaTypeDefinition_isEnumerationType"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-complex
                              "blpapi_SchemaTypeDefinition_isComplex"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-simple
                              "blpapi_SchemaTypeDefinition_isSimple"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-is-enumeration
                              "blpapi_SchemaTypeDefinition_isEnumeration"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-num-element-definitions
                              "blpapi_SchemaTypeDefinition_numElementDefinitions"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-get-element-definition
                              "blpapi_SchemaTypeDefinition_getElementDefinition"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             (:pointer
                              blpapi-schema-element-definition-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-get-element-definition-at
                              "blpapi_SchemaTypeDefinition_getElementDefinitionAt"
                              :source)
                             ((type
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t)))
                              (index size-t))
                             :result-type
                             (:pointer
                              blpapi-schema-element-definition-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-print
                              "blpapi_SchemaTypeDefinition_print"
                              :source)
                             ((element
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (user-stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-set-user-data
                              "blpapi_SchemaTypeDefinition_setUserData"
                              :source)
                             ((element
                               (:pointer
                                blpapi-schema-type-definition-t))
                              (userdata (:pointer :void)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-user-data
                              "blpapi_SchemaTypeDefinition_userData"
                              :source)
                             ((element
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             (:pointer :void)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-schema-type-definition-enumeration
                              "blpapi_SchemaTypeDefinition_enumeration"
                              :source)
                             ((element
                               (:pointer
                                (:const
                                 blpapi-schema-type-definition-t))))
                             :result-type
                             (:pointer blpapi-constant-list-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_element.h"

(fli:define-foreign-function (blpapi-element-name
                              "blpapi_Element_name"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-name-string
                              "blpapi_Element_nameString"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-definition
                              "blpapi_Element_definition"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             (:pointer
                              blpapi-schema-element-definition-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-datatype
                              "blpapi_Element_datatype"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-is-complex-type
                              "blpapi_Element_isComplexType"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-is-array
                              "blpapi_Element_isArray"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-is-read-only
                              "blpapi_Element_isReadOnly"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-num-values
                              "blpapi_Element_numValues"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-num-elements
                              "blpapi_Element_numElements"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-is-null-value
                              "blpapi_Element_isNullValue"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (position size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-is-null
                              "blpapi_Element_isNull"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-print
                              "blpapi_Element_print"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-element-at
                              "blpapi_Element_getElementAt"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (result
                               (:reference-return (:pointer blpapi-element-t)))
                              (position size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-element
                              "blpapi_Element_getElement"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (result
                               (:reference-return (:pointer blpapi-element-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-has-element
                              "blpapi_Element_hasElement"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-has-element-ex
                              "blpapi_Element_hasElementEx"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (exclude-null-elements :int)
                              (reserved :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-bool
                              "blpapi_Element_getValueAsBool"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-bool-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-char
                              "blpapi_Element_getValueAsChar"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-char-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-int32
                              "blpapi_Element_getValueAsInt32"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-int32-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-int64
                              "blpapi_Element_getValueAsInt64"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-int64-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-float32
                              "blpapi_Element_getValueAsFloat32"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-float32-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-float64
                              "blpapi_Element_getValueAsFloat64"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-float64-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-string
                              "blpapi_Element_getValueAsString"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer
                               (:reference-return (:reference :ef-mb-string :allow-null t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-datetime
                              "blpapi_Element_getValueAsDatetime"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer (:reference-return blpapi-datetime-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-high-precision-datetime
                              "blpapi_Element_getValueAsHighPrecisionDatetime"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer
                               (:reference-return
                                blpapi-high-precision-datetime-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-element
                              "blpapi_Element_getValueAsElement"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer
                               (:reference-return (:pointer blpapi-element-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-value-as-name
                              "blpapi_Element_getValueAsName"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (buffer
                               (:reference-return (:pointer blpapi-name-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-get-choice
                              "blpapi_Element_getChoice"
                              :source)
                             ((element
                               (:pointer (:const blpapi-element-t)))
                              (result
                               (:reference-return (:pointer blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-bool
                              "blpapi_Element_setValueBool"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-bool-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-char
                              "blpapi_Element_setValueChar"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-char-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-int32
                              "blpapi_Element_setValueInt32"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-int32-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-int64
                              "blpapi_Element_setValueInt64"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-int64-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-float32
                              "blpapi_Element_setValueFloat32"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-float32-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-float64
                              "blpapi_Element_setValueFloat64"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value blpapi-float64-t)
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-string
                              "blpapi_Element_setValueString"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value (:reference :ef-mb-string :allow-null t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-datetime
                              "blpapi_Element_setValueDatetime"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value
                               (:pointer (:const blpapi-datetime-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-high-precision-datetime
                              "blpapi_Element_setValueHighPrecisionDatetime"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value
                               (:pointer
                                (:const
                                 blpapi-high-precision-datetime-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-from-element
                              "blpapi_Element_setValueFromElement"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value (:pointer blpapi-element-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-value-from-name
                              "blpapi_Element_setValueFromName"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (value (:pointer (:const blpapi-name-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-bool
                              "blpapi_Element_setElementBool"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-bool-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-char
                              "blpapi_Element_setElementChar"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-char-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-int32
                              "blpapi_Element_setElementInt32"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-int32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-int64
                              "blpapi_Element_setElementInt64"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-int64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-float32
                              "blpapi_Element_setElementFloat32"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-float32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-float64
                              "blpapi_Element_setElementFloat64"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value blpapi-float64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-string
                              "blpapi_Element_setElementString"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-datetime
                              "blpapi_Element_setElementDatetime"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value
                               (:pointer (:const blpapi-datetime-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-high-precision-datetime
                              "blpapi_Element_setElementHighPrecisionDatetime"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (value
                               (:pointer
                                (:const
                                 blpapi-high-precision-datetime-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-from-field
                              "blpapi_Element_setElementFromField"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (sourcebuffer
                               (:pointer blpapi-element-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-element-from-name
                              "blpapi_Element_setElementFromName"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (element-name (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (buffer
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-append-element
                              "blpapi_Element_appendElement"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (appended-element
                               (:reference-return (:pointer blpapi-element-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-element-set-choice
                              "blpapi_Element_setChoice"
                              :source)
                             ((element (:pointer blpapi-element-t))
                              (result-element
                               (:reference-return (:pointer blpapi-element-t)))
                              (name-cstr (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_request.h"

(fli:define-c-struct (blpapi-request
                      (:foreign-name "blpapi_Request")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-request-t
                       (:foreign-name "blpapi_Request_t"))
                      (:struct blpapi-request))
(fli:define-foreign-function (blpapi-request-destroy
                              "blpapi_Request_destroy"
                              :source)
                             ((request (:pointer blpapi-request-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-request-elements
                              "blpapi_Request_elements"
                              :source)
                             ((request (:pointer blpapi-request-t)))
                             :result-type
                             (:pointer blpapi-element-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-request-set-preferred-route
                              "blpapi_Request_setPreferredRoute"
                              :source)
                             ((request (:pointer blpapi-request-t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_service.h"

(fli:define-foreign-function (blpapi-operation-name
                              "blpapi_Operation_name"
                              :source)
                             ((service (:pointer blpapi-operation-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-operation-description
                              "blpapi_Operation_description"
                              :source)
                             ((service (:pointer blpapi-operation-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-operation-request-definition
                              "blpapi_Operation_requestDefinition"
                              :source)
                             ((service (:pointer blpapi-operation-t))
                              (request-definition
                               (:reference-return
                                (:pointer
                                 blpapi-schema-element-definition-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-operation-num-response-definitions
                              "blpapi_Operation_numResponseDefinitions"
                              :source)
                             ((service (:pointer blpapi-operation-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-operation-response-definition
                              "blpapi_Operation_responseDefinition"
                              :source)
                             ((service (:pointer blpapi-operation-t))
                              (response-definition
                               (:reference-return
                                (:pointer
                                 blpapi-schema-element-definition-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-name
                              "blpapi_Service_name"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-description
                              "blpapi_Service_description"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-num-operations
                              "blpapi_Service_numOperations"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-num-event-definitions
                              "blpapi_Service_numEventDefinitions"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-add-ref
                              "blpapi_Service_addRef"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-release
                              "blpapi_Service_release"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-authorization-service-name
                              "blpapi_Service_authorizationServiceName"
                              :source)
                             ((service (:pointer blpapi-service-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-get-operation
                              "blpapi_Service_getOperation"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (operation
                               (:reference-return
                                (:pointer blpapi-operation-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-get-operation-at
                              "blpapi_Service_getOperationAt"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (operation
                               (:reference-return
                                (:pointer blpapi-operation-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-get-event-definition
                              "blpapi_Service_getEventDefinition"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (result
                               (:reference-return
                                (:pointer
                                 blpapi-schema-element-definition-t)))
                              (name-string (:reference :ef-mb-string :allow-null t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-get-event-definition-at
                              "blpapi_Service_getEventDefinitionAt"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (result
                               (:reference-return
                                (:pointer
                                 blpapi-schema-element-definition-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-create-request
                              "blpapi_Service_createRequest"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (request
                               (:reference-return (:pointer blpapi-request-t)))
                              (operation (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-create-authorization-request
                              "blpapi_Service_createAuthorizationRequest"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (request
                               (:reference-return (:pointer blpapi-request-t)))
                              (operation (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-create-publish-event
                              "blpapi_Service_createPublishEvent"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (event
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-create-admin-event
                              "blpapi_Service_createAdminEvent"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (event
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-create-response-event
                              "blpapi_Service_createResponseEvent"
                              :source)
                             ((service (:pointer blpapi-service-t))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (event
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-print
                              "blpapi_Service_print"
                              :source)
                             ((service
                               (:pointer (:const blpapi-service-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (stream (:pointer :void))
                              (level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_message.h"

(fli:define-c-struct (blpapi-message
                      (:foreign-name "blpapi_Message")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-message-t
                       (:foreign-name "blpapi_Message_t"))
                      (:struct blpapi-message))
(fli:define-foreign-function (blpapi-message-message-type
                              "blpapi_Message_messageType"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             (:pointer blpapi-name-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-type-string
                              "blpapi_Message_typeString"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-topic-name
                              "blpapi_Message_topicName"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-service
                              "blpapi_Message_service"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             (:pointer blpapi-service-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-num-correlation-ids
                              "blpapi_Message_numCorrelationIds"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-correlation-id
                              "blpapi_Message_correlationId"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t)))
                              (index size-t))
                             :result-type
                             blpapi-correlation-id-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-elements
                              "blpapi_Message_elements"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             (:pointer blpapi-element-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-private-data
                              "blpapi_Message_privateData"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t)))
                              (size (:pointer size-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-fragment-type
                              "blpapi_Message_fragmentType"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-recap-type
                              "blpapi_Message_recapType"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-print
                              "blpapi_Message_print"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t)))
                              (stream-writer blpapi-stream-writer-t)
                              (stream (:pointer :void))
                              (indent-level :int)
                              (spaces-per-level :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-add-ref
                              "blpapi_Message_addRef"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-release
                              "blpapi_Message_release"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-time-received
                              "blpapi_Message_timeReceived"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t)))
                              (time-received
                               (:pointer blpapi-time-point-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_event.h"

(fli:define-foreign-function (blpapi-event-event-type
                              "blpapi_Event_eventType"
                              :source)
                             ((event
                               (:pointer (:const blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-add-ref
                              "blpapi_Event_addRef"
                              :source)
                             ((event
                               (:pointer (:const blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-release
                              "blpapi_Event_release"
                              :source)
                             ((event
                               (:pointer (:const blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-queue-create
                              "blpapi_EventQueue_create"
                              :source)
                             nil
                             :result-type
                             (:pointer blpapi-event-queue-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-queue-destroy
                              "blpapi_EventQueue_destroy"
                              :source)
                             ((event-queue
                               (:pointer blpapi-event-queue-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-queue-next-event
                              "blpapi_EventQueue_nextEvent"
                              :source)
                             ((event-queue
                               (:pointer blpapi-event-queue-t))
                              (timeout :int))
                             :result-type
                             (:pointer blpapi-event-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-queue-purge
                              "blpapi_EventQueue_purge"
                              :source)
                             ((event-queue
                               (:pointer blpapi-event-queue-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-queue-try-next-event
                              "blpapi_EventQueue_tryNextEvent"
                              :source)
                             ((event-queue
                               (:pointer blpapi-event-queue-t))
                              (event-pointer
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-iterator-create
                              "blpapi_MessageIterator_create"
                              :source)
                             ((event
                               (:pointer (:const blpapi-event-t))))
                             :result-type
                             (:pointer blpapi-message-iterator-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-iterator-destroy
                              "blpapi_MessageIterator_destroy"
                              :source)
                             ((iterator
                               (:pointer blpapi-message-iterator-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-message-iterator-next
                              "blpapi_MessageIterator_next"
                              :source)
                             ((iterator
                               (:pointer blpapi-message-iterator-t))
                              (result
                               (:reference-return (:pointer blpapi-message-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_eventdispatcher.h"

(fli:define-foreign-function (blpapi-event-dispatcher-create
                              "blpapi_EventDispatcher_create"
                              :source)
                             ((num-dispatcher-threads size-t))
                             :result-type
                             (:pointer blpapi-event-dispatcher-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-dispatcher-destroy
                              "blpapi_EventDispatcher_destroy"
                              :source)
                             ((handle
                               (:pointer blpapi-event-dispatcher-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-dispatcher-start
                              "blpapi_EventDispatcher_start"
                              :source)
                             ((handle
                               (:pointer blpapi-event-dispatcher-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-dispatcher-stop
                              "blpapi_EventDispatcher_stop"
                              :source)
                             ((handle
                               (:pointer blpapi-event-dispatcher-t))
                              (async :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-dispatcher-dispatch-events
                              "blpapi_EventDispatcher_dispatchEvents"
                              :source)
                             ((handle
                               (:pointer blpapi-event-dispatcher-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_identity.h"

(fli:define-foreign-function (blpapi-identity-release
                              "blpapi_Identity_release"
                              :source)
                             ((handle (:pointer blpapi-identity-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-identity-add-ref
                              "blpapi_Identity_addRef"
                              :source)
                             ((handle (:pointer blpapi-identity-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-identity-has-entitlements
                              "blpapi_Identity_hasEntitlements"
                              :source)
                             ((handle
                               (:pointer (:const blpapi-identity-t)))
                              (service
                               (:pointer (:const blpapi-service-t)))
                              (eid-element
                               (:pointer (:const blpapi-element-t)))
                              (entitlement-ids
                               (:pointer (:const :int)))
                              (num-entitlements size-t)
                              (failed-entitlements (:pointer :int))
                              (failed-entitlements-count
                               (:pointer :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-identity-is-authorized
                              "blpapi_Identity_isAuthorized"
                              :source)
                             ((handle
                               (:pointer (:const blpapi-identity-t)))
                              (service
                               (:pointer (:const blpapi-service-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-identity-get-seat-type
                              "blpapi_Identity_getSeatType"
                              :source)
                             ((handle
                               (:pointer (:const blpapi-identity-t)))
                              (seat-type (:pointer :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_abstractsession.h"

(fli:define-foreign-function (blpapi-user-handle-release
                              "blpapi_UserHandle_release"
                              :source)
                             ((handle (:pointer blpapi-user-handle-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-user-handle-add-ref
                              "blpapi_UserHandle_addRef"
                              :source)
                             ((handle (:pointer blpapi-user-handle-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-user-handle-has-entitlements
                              "blpapi_UserHandle_hasEntitlements"
                              :source)
                             ((handle
                               (:pointer
                                (:const blpapi-user-handle-t)))
                              (service
                               (:pointer (:const blpapi-service-t)))
                              (eid-element
                               (:pointer (:const blpapi-element-t)))
                              (entitlement-ids
                               (:pointer (:const :int)))
                              (num-entitlements size-t)
                              (failed-entitlements (:pointer :int))
                              (failed-entitlements-count
                               (:pointer :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-cancel
                              "blpapi_AbstractSession_cancel"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (correlation-ids
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (num-correlation-ids size-t)
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-send-authorization-request
                              "blpapi_AbstractSession_sendAuthorizationRequest"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (request
                               (:pointer (:const blpapi-request-t)))
                              (identity (:pointer blpapi-identity-t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (event-queue
                               (:pointer blpapi-event-queue-t))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-open-service
                              "blpapi_AbstractSession_openService"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (service-identifier
                               (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-open-service-async
                              "blpapi_AbstractSession_openServiceAsync"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (service-identifier
                               (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-generate-token
                              "blpapi_AbstractSession_generateToken"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (event-queue
                               (:pointer blpapi-event-queue-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-get-service
                              "blpapi_AbstractSession_getService"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t))
                              (service
                               (:reference-return (:pointer blpapi-service-t)))
                              (service-identifier
                               (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-abstract-session-create-identity
                              "blpapi_AbstractSession_createIdentity"
                              :source)
                             ((session
                               (:pointer blpapi-abstract-session-t)))
                             :result-type
                             (:pointer blpapi-identity-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_diagnosticsutil.h"

(fli:define-foreign-function (blpapi-diagnostics-util-memory-info
                              "blpapi_DiagnosticsUtil_memoryInfo"
                              :source)
                             ((buffer (:reference-return :char))
                              (buffer-length size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_topic.h"

(fli:define-c-struct (blpapi-topic
                      (:foreign-name "blpapi_Topic")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-topic-t (:foreign-name "blpapi_Topic_t"))
                      (:struct blpapi-topic))
(fli:define-foreign-function (blpapi-topic-create
                              "blpapi_Topic_create"
                              :source)
                             ((from (:pointer blpapi-topic-t)))
                             :result-type
                             (:pointer blpapi-topic-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-destroy
                              "blpapi_Topic_destroy"
                              :source)
                             ((victim (:pointer blpapi-topic-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-compare
                              "blpapi_Topic_compare"
                              :source)
                             ((lhs (:pointer (:const blpapi-topic-t)))
                              (rhs (:pointer (:const blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-service
                              "blpapi_Topic_service"
                              :source)
                             ((topic
                               (:pointer (:const blpapi-topic-t))))
                             :result-type
                             (:pointer blpapi-service-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-is-active
                              "blpapi_Topic_isActive"
                              :source)
                             ((topic
                               (:pointer (:const blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_eventformatter.h"

(fli:define-foreign-function (blpapi-event-formatter-create
                              "blpapi_EventFormatter_create"
                              :source)
                             ((event (:pointer blpapi-event-t)))
                             :result-type
                             (:pointer blpapi-event-formatter-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-destroy
                              "blpapi_EventFormatter_destroy"
                              :source)
                             ((victim
                               (:pointer blpapi-event-formatter-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-message
                              "blpapi_EventFormatter_appendMessage"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name (:pointer blpapi-name-t))
                              (topic
                               (:pointer (:const blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-message-seq
                              "blpapi_EventFormatter_appendMessageSeq"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name (:pointer blpapi-name-t))
                              (topic
                               (:pointer (:const blpapi-topic-t)))
                              (sequence-number (:unsigned :int))
                              (arg-6 (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-response
                              "blpapi_EventFormatter_appendResponse"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name (:pointer blpapi-name-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-recap-message
                              "blpapi_EventFormatter_appendRecapMessage"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (topic
                               (:pointer (:const blpapi-topic-t)))
                              (cid
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-recap-message-seq
                              "blpapi_EventFormatter_appendRecapMessageSeq"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (topic
                               (:pointer (:const blpapi-topic-t)))
                              (cid
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (sequence-number (:unsigned :int))
                              (arg-5 (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-fragmented-recap-message
                              "blpapi_EventFormatter_appendFragmentedRecapMessage"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name (:pointer blpapi-name-t))
                              (topic
                               (:pointer (:const blpapi-topic-t)))
                              (cid
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (fragment-type :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-fragmented-recap-message-seq
                              "blpapi_EventFormatter_appendFragmentedRecapMessageSeq"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name (:pointer blpapi-name-t))
                              (topic
                               (:pointer (:const blpapi-topic-t)))
                              (fragment-type :int)
                              (sequence-number (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-bool
                              "blpapi_EventFormatter_setValueBool"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value blpapi-bool-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-char
                              "blpapi_EventFormatter_setValueChar"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value :char))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-int32
                              "blpapi_EventFormatter_setValueInt32"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value blpapi-int32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-int64
                              "blpapi_EventFormatter_setValueInt64"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value blpapi-int64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-float32
                              "blpapi_EventFormatter_setValueFloat32"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value blpapi-float32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-float64
                              "blpapi_EventFormatter_setValueFloat64"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value blpapi-float64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-datetime
                              "blpapi_EventFormatter_setValueDatetime"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value
                               (:pointer (:const blpapi-datetime-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-string
                              "blpapi_EventFormatter_setValueString"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-from-name
                              "blpapi_EventFormatter_setValueFromName"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t)))
                              (value
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-set-value-null
                              "blpapi_EventFormatter_setValueNull"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-push-element
                              "blpapi_EventFormatter_pushElement"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (type-string (:reference :ef-mb-string :allow-null t))
                              (type-name
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-pop-element
                              "blpapi_EventFormatter_popElement"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-bool
                              "blpapi_EventFormatter_appendValueBool"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value blpapi-bool-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-char
                              "blpapi_EventFormatter_appendValueChar"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value :char))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-int32
                              "blpapi_EventFormatter_appendValueInt32"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value blpapi-int32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-int64
                              "blpapi_EventFormatter_appendValueInt64"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value blpapi-int64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-float32
                              "blpapi_EventFormatter_appendValueFloat32"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value blpapi-float32-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-float64
                              "blpapi_EventFormatter_appendValueFloat64"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value blpapi-float64-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-datetime
                              "blpapi_EventFormatter_appendValueDatetime"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value
                               (:pointer (:const blpapi-datetime-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-string
                              "blpapi_EventFormatter_appendValueString"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-value-from-name
                              "blpapi_EventFormatter_appendValueFromName"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t))
                              (value
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-event-formatter-append-element
                              "blpapi_EventFormatter_appendElement"
                              :source)
                             ((formatter (:pointer
                                          blpapi-event-formatter-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_highresolutionclock.h"

(fli:define-foreign-function (blpapi-high-resolution-clock-now
                              "blpapi_HighResolutionClock_now"
                              :source)
                             ((time-point
                               (:pointer blpapi-time-point-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_logging.h"

(fli:define-c-typedef (blpapi-logging-func-t
                       (:foreign-name "blpapi_Logging_Func_t"))
                      (:pointer
                       (:function
                        (blpapi-uint64-t
                         :int
                         blpapi-datetime-t
                         (:reference :ef-mb-string :allow-null t)
                         (:reference :ef-mb-string :allow-null t))
                        :void
                        :calling-convention
                        :cdecl)))
(fli:define-foreign-function (blpapi-logging-register-callback
                              "blpapi_Logging_registerCallback"
                              :source)
                             ((callback blpapi-logging-func-t)
                              (threshold-severity
                               blpapi-logging-severity-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-logging-log-test-message
                              "blpapi_Logging_logTestMessage"
                              :source)
                             ((severity blpapi-logging-severity-t))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_resolutionlist.h"

(fli:define-c-struct (blpapi-resolution-list
                      (:foreign-name "blpapi_ResolutionList")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-resolution-list-t
                       (:foreign-name "blpapi_ResolutionList_t"))
                      (:struct blpapi-resolution-list))
(fli:define-foreign-function (blpapi-resolution-list-extract-attribute-from-resolution-success
                              "blpapi_ResolutionList_extractAttributeFromResolutionSuccess"
                              :source)
                             ((message
                               (:pointer (:const blpapi-message-t)))
                              (attribute
                               (:pointer (:const blpapi-name-t))))
                             :result-type
                             (:pointer blpapi-element-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-create
                              "blpapi_ResolutionList_create"
                              :source)
                             ((from
                               (:pointer blpapi-resolution-list-t)))
                             :result-type
                             (:pointer blpapi-resolution-list-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-destroy
                              "blpapi_ResolutionList_destroy"
                              :source)
                             ((list (:pointer
                                     blpapi-resolution-list-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-add
                              "blpapi_ResolutionList_add"
                              :source)
                             ((list (:pointer
                                     blpapi-resolution-list-t))
                              (topic (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-add-from-message
                              "blpapi_ResolutionList_addFromMessage"
                              :source)
                             ((list (:pointer
                                     blpapi-resolution-list-t))
                              (topic
                               (:pointer (:const blpapi-message-t)))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-add-attribute
                              "blpapi_ResolutionList_addAttribute"
                              :source)
                             ((list (:pointer
                                     blpapi-resolution-list-t))
                              (name (:pointer (:const blpapi-name-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-correlation-id-at
                              "blpapi_ResolutionList_correlationIdAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (result
                               (:pointer blpapi-correlation-id-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-topic-string
                              "blpapi_ResolutionList_topicString"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (topic
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-topic-string-at
                              "blpapi_ResolutionList_topicStringAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (topic
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-status
                              "blpapi_ResolutionList_status"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (status (:pointer :int))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-status-at
                              "blpapi_ResolutionList_statusAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (status (:pointer :int))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-attribute
                              "blpapi_ResolutionList_attribute"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-element-t)))
                              (attribute
                               (:pointer (:const blpapi-name-t)))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-attribute-at
                              "blpapi_ResolutionList_attributeAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-element-t)))
                              (attribute
                               (:pointer (:const blpapi-name-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-message
                              "blpapi_ResolutionList_message"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-message-t)))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-message-at
                              "blpapi_ResolutionList_messageAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-message-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-resolution-list-size
                              "blpapi_ResolutionList_size"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-resolution-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_tlsoptions.h"

(fli:define-foreign-function (blpapi-tls-options-create
                              "blpapi_TlsOptions_create"
                              :source)
                             nil
                             :result-type
                             (:pointer blpapi-tls-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-duplicate
                              "blpapi_TlsOptions_duplicate"
                              :source)
                             ((parameters
                               (:pointer
                                (:const blpapi-tls-options-t))))
                             :result-type
                             (:pointer blpapi-tls-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-copy
                              "blpapi_TlsOptions_copy"
                              :source)
                             ((lhs (:pointer blpapi-tls-options-t))
                              (rhs
                               (:pointer
                                (:const blpapi-tls-options-t))))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-destroy
                              "blpapi_TlsOptions_destroy"
                              :source)
                             ((parameters
                               (:pointer blpapi-tls-options-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-create-from-files
                              "blpapi_TlsOptions_createFromFiles"
                              :source)
                             ((client-credentials-file-name
                               (:reference :ef-mb-string :allow-null t))
                              (client-credentials-password
                               (:reference :ef-mb-string :allow-null t))
                              (trusted-certificates-file-name
                               (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             (:pointer blpapi-tls-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-create-from-blobs
                              "blpapi_TlsOptions_createFromBlobs"
                              :source)
                             ((client-credentials-raw-data
                               (:reference :ef-mb-string :allow-null t))
                              (client-credentials-raw-data-length :int)
                              (client-credentials-password
                               (:reference :ef-mb-string :allow-null t))
                              (trusted-certificates-raw-data
                               (:reference :ef-mb-string :allow-null t))
                              (trusted-certificates-raw-data-length
                               :int))
                             :result-type
                             (:pointer blpapi-tls-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-set-tls-handshake-timeout-ms
                              "blpapi_TlsOptions_setTlsHandshakeTimeoutMs"
                              :source)
                             ((paramaters
                               (:pointer blpapi-tls-options-t))
                              (tls-handshake-timeout-ms :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-tls-options-set-crl-fetch-timeout-ms
                              "blpapi_TlsOptions_setCrlFetchTimeoutMs"
                              :source)
                             ((paramaters
                               (:pointer blpapi-tls-options-t))
                              (crl-fetch-timeout-ms :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_sessionoptions.h"

(fli:define-foreign-function (blpapi-session-options-create
                              "blpapi_SessionOptions_create"
                              :source)
                             nil
                             :result-type
                             (:pointer blpapi-session-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-duplicate
                              "blpapi_SessionOptions_duplicate"
                              :source)
                             ((parameters
                               (:pointer
                                (:const blpapi-session-options-t))))
                             :result-type
                             (:pointer blpapi-session-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-copy
                              "blpapi_SessionOptions_copy"
                              :source)
                             ((lhs (:pointer blpapi-session-options-t))
                              (rhs
                               (:pointer
                                (:const blpapi-session-options-t))))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-destroy
                              "blpapi_SessionOptions_destroy"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-server-host
                              "blpapi_SessionOptions_setServerHost"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (server-host (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-server-port
                              "blpapi_SessionOptions_setServerPort"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (server-port (:unsigned :short)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-server-address
                              "blpapi_SessionOptions_setServerAddress"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (server-host (:reference :ef-mb-string :allow-null t))
                              (server-port (:unsigned :short))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-remove-server-address
                              "blpapi_SessionOptions_removeServerAddress"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-connect-timeout
                              "blpapi_SessionOptions_setConnectTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (timeout-in-milliseconds
                               (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-default-services
                              "blpapi_SessionOptions_setDefaultServices"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (default-services
                               (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-default-subscription-service
                              "blpapi_SessionOptions_setDefaultSubscriptionService"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (service-identifier
                               (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-default-topic-prefix
                              "blpapi_SessionOptions_setDefaultTopicPrefix"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (prefix (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-allow-multiple-correlators-per-msg
                              "blpapi_SessionOptions_setAllowMultipleCorrelatorsPerMsg"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (allow-multiple-correlators-per-msg
                               :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-client-mode
                              "blpapi_SessionOptions_setClientMode"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (client-mode :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-max-pending-requests
                              "blpapi_SessionOptions_setMaxPendingRequests"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (max-pending-requests :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-auto-restart-on-disconnection
                              "blpapi_SessionOptions_setAutoRestartOnDisconnection"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (auto-restart :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-auto-restart
                              "blpapi_SessionOptions_setAutoRestart"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (auto-restart :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-authentication-options
                              "blpapi_SessionOptions_setAuthenticationOptions"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (auth-options (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-num-start-attempts
                              "blpapi_SessionOptions_setNumStartAttempts"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (num-start-attempts :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-max-event-queue-size
                              "blpapi_SessionOptions_setMaxEventQueueSize"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (max-event-queue-size size-t))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-slow-consumer-warning-hi-water-mark
                              "blpapi_SessionOptions_setSlowConsumerWarningHiWaterMark"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (hi-water-mark :float))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-slow-consumer-warning-lo-water-mark
                              "blpapi_SessionOptions_setSlowConsumerWarningLoWaterMark"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (lo-water-mark :float))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-default-keep-alive-inactivity-time
                              "blpapi_SessionOptions_setDefaultKeepAliveInactivityTime"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (inactivity-msecs :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-default-keep-alive-response-timeout
                              "blpapi_SessionOptions_setDefaultKeepAliveResponseTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (timeout-msecs :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-keep-alive-enabled
                              "blpapi_SessionOptions_setKeepAliveEnabled"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (is-enabled :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-record-subscription-data-receive-times
                              "blpapi_SessionOptions_setRecordSubscriptionDataReceiveTimes"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (should-record :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-service-check-timeout
                              "blpapi_SessionOptions_setServiceCheckTimeout"
                              :source)
                             ((paramaters
                               (:pointer blpapi-session-options-t))
                              (timeout-msecs :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-service-download-timeout
                              "blpapi_SessionOptions_setServiceDownloadTimeout"
                              :source)
                             ((paramaters
                               (:pointer blpapi-session-options-t))
                              (timeout-msecs :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-set-tls-options
                              "blpapi_SessionOptions_setTlsOptions"
                              :source)
                             ((paramaters
                               (:pointer blpapi-session-options-t))
                              (tls-options
                               (:pointer
                                (:const blpapi-tls-options-t))))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-server-host
                              "blpapi_SessionOptions_serverHost"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-server-port
                              "blpapi_SessionOptions_serverPort"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:unsigned :int)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-num-server-addresses
                              "blpapi_SessionOptions_numServerAddresses"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-get-server-address
                              "blpapi_SessionOptions_getServerAddress"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (server-host
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (server-port
                               (:pointer (:unsigned :short)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-connect-timeout
                              "blpapi_SessionOptions_connectTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:unsigned :int)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-default-services
                              "blpapi_SessionOptions_defaultServices"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-default-subscription-service
                              "blpapi_SessionOptions_defaultSubscriptionService"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-default-topic-prefix
                              "blpapi_SessionOptions_defaultTopicPrefix"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-allow-multiple-correlators-per-msg
                              "blpapi_SessionOptions_allowMultipleCorrelatorsPerMsg"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-client-mode
                              "blpapi_SessionOptions_clientMode"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-max-pending-requests
                              "blpapi_SessionOptions_maxPendingRequests"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-auto-restart-on-disconnection
                              "blpapi_SessionOptions_autoRestartOnDisconnection"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-auto-restart
                              "blpapi_SessionOptions_autoRestart"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-authentication-options
                              "blpapi_SessionOptions_authenticationOptions"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-num-start-attempts
                              "blpapi_SessionOptions_numStartAttempts"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-max-event-queue-size
                              "blpapi_SessionOptions_maxEventQueueSize"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             size-t
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-slow-consumer-warning-hi-water-mark
                              "blpapi_SessionOptions_slowConsumerWarningHiWaterMark"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :float
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-slow-consumer-warning-lo-water-mark
                              "blpapi_SessionOptions_slowConsumerWarningLoWaterMark"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :float
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-default-keep-alive-inactivity-time
                              "blpapi_SessionOptions_defaultKeepAliveInactivityTime"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-default-keep-alive-response-timeout
                              "blpapi_SessionOptions_defaultKeepAliveResponseTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-keep-alive-enabled
                              "blpapi_SessionOptions_keepAliveEnabled"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-record-subscription-data-receive-times
                              "blpapi_SessionOptions_recordSubscriptionDataReceiveTimes"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-service-check-timeout
                              "blpapi_SessionOptions_serviceCheckTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-options-service-download-timeout
                              "blpapi_SessionOptions_serviceDownloadTimeout"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_subscriptionlist.h"

(fli:define-c-struct (blpapi-subscription-list
                      (:foreign-name "blpapi_SubscriptionList")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-subscription-list-t
                       (:foreign-name "blpapi_SubscriptionList_t"))
                      (:struct blpapi-subscription-list))
(fli:define-foreign-function (blpapi-subscription-list-create
                              "blpapi_SubscriptionList_create"
                              :source)
                             nil
                             :result-type
                             (:pointer blpapi-subscription-list-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-destroy
                              "blpapi_SubscriptionList_destroy"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-add
                              "blpapi_SubscriptionList_add"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t))
                              (subscription-string
                               (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (fields
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (options
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (numfields size-t)
                              (num-options size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-add-resolved
                              "blpapi_SubscriptionList_addResolved"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t))
                              (subscription-string
                               (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-clear
                              "blpapi_SubscriptionList_clear"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-append
                              "blpapi_SubscriptionList_append"
                              :source)
                             ((dest
                               (:pointer blpapi-subscription-list-t))
                              (src
                               (:pointer
                                (:const blpapi-subscription-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-size
                              "blpapi_SubscriptionList_size"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-subscription-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-correlation-id-at
                              "blpapi_SubscriptionList_correlationIdAt"
                              :source)
                             ((list (:pointer
                                     (:const
                                      blpapi-subscription-list-t)))
                              (result
                               (:pointer blpapi-correlation-id-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-topic-string-at
                              "blpapi_SubscriptionList_topicStringAt"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t))
                              (result
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-list-is-resolved-at
                              "blpapi_SubscriptionList_isResolvedAt"
                              :source)
                             ((list (:pointer
                                     blpapi-subscription-list-t))
                              (result (:pointer :int))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "C:\\Users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1\\blpapi_topiclist.h"

(fli:define-c-struct (blpapi-topic-list
                      (:foreign-name "blpapi_TopicList")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-topic-list-t
                       (:foreign-name "blpapi_TopicList_t"))
                      (:struct blpapi-topic-list))
(fli:define-foreign-function (blpapi-topic-list-create
                              "blpapi_TopicList_create"
                              :source)
                             ((from (:pointer blpapi-topic-list-t)))
                             :result-type
                             (:pointer blpapi-topic-list-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-destroy
                              "blpapi_TopicList_destroy"
                              :source)
                             ((list (:pointer blpapi-topic-list-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-add
                              "blpapi_TopicList_add"
                              :source)
                             ((list (:pointer blpapi-topic-list-t))
                              (topic (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-add-from-message
                              "blpapi_TopicList_addFromMessage"
                              :source)
                             ((list (:pointer blpapi-topic-list-t))
                              (topic
                               (:pointer (:const blpapi-message-t)))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-correlation-id-at
                              "blpapi_TopicList_correlationIdAt"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (result
                               (:pointer blpapi-correlation-id-t))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-topic-string
                              "blpapi_TopicList_topicString"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (topic
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-topic-string-at
                              "blpapi_TopicList_topicStringAt"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (topic
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-status
                              "blpapi_TopicList_status"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (status (:pointer :int))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-status-at
                              "blpapi_TopicList_statusAt"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (status (:pointer :int))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-message
                              "blpapi_TopicList_message"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-message-t)))
                              (id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-message-at
                              "blpapi_TopicList_messageAt"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t)))
                              (element
                               (:reference-return (:pointer blpapi-message-t)))
                              (index size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-topic-list-size
                              "blpapi_TopicList_size"
                              :source)
                             ((list (:pointer
                                     (:const blpapi-topic-list-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_providersession.h"

(fli:define-c-struct (blpapi-service-registration-options
                      (:foreign-name
                       "blpapi_ServiceRegistrationOptions")
                      (:forward-reference t)))
(fli:define-c-typedef (blpapi-service-registration-options-t
                       (:foreign-name
                        "blpapi_ServiceRegistrationOptions_t"))
                      (:struct blpapi-service-registration-options))
(fli:define-c-typedef (blpapi-provider-event-handler-t
                       (:foreign-name "blpapi_ProviderEventHandler_t"))
                      (:pointer
                       (:function
                        ((:pointer blpapi-event-t)
                         (:pointer blpapi-provider-session-t)
                         (:pointer :void))
                        :void
                        :calling-convention
                        :cdecl)))
(fli:define-foreign-function (blpapi-provider-session-create
                              "blpapi_ProviderSession_create"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (handler blpapi-provider-event-handler-t)
                              (dispatcher
                               (:pointer blpapi-event-dispatcher-t))
                              (user-data (:pointer :void)))
                             :result-type
                             (:pointer blpapi-provider-session-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-destroy
                              "blpapi_ProviderSession_destroy"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-start
                              "blpapi_ProviderSession_start"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-start-async
                              "blpapi_ProviderSession_startAsync"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-stop
                              "blpapi_ProviderSession_stop"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-stop-async
                              "blpapi_ProviderSession_stopAsync"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-next-event
                              "blpapi_ProviderSession_nextEvent"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (event-pointer
                               (:reference-return (:pointer blpapi-event-t)))
                              (timeout-in-milliseconds
                               (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-try-next-event
                              "blpapi_ProviderSession_tryNextEvent"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (event-pointer
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-register-service
                              "blpapi_ProviderSession_registerService"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t))
                              (identity (:pointer
                                         (:const blpapi-identity-t)))
                              (registration-options
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-activate-sub-service-code-range
                              "blpapi_ProviderSession_activateSubServiceCodeRange"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t))
                              (begin :int)
                              (end :int)
                              (priority :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-deactivate-sub-service-code-range
                              "blpapi_ProviderSession_deactivateSubServiceCodeRange"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t))
                              (begin :int)
                              (end :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-register-service-async
                              "blpapi_ProviderSession_registerServiceAsync"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t))
                              (identity (:pointer
                                         (:const blpapi-identity-t)))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (registration-options
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-deregister-service
                              "blpapi_ProviderSession_deregisterService"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-resolve
                              "blpapi_ProviderSession_resolve"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (resolution-list
                               (:pointer blpapi-resolution-list-t))
                              (resolve-mode :int)
                              (identity (:pointer
                                         (:const blpapi-identity-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-resolve-async
                              "blpapi_ProviderSession_resolveAsync"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (resolution-list
                               (:pointer
                                (:const blpapi-resolution-list-t)))
                              (resolve-mode :int)
                              (identity (:pointer
                                         (:const blpapi-identity-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-create-topics
                              "blpapi_ProviderSession_createTopics"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (topic-list
                               (:pointer blpapi-topic-list-t))
                              (resolve-mode :int)
                              (identity (:pointer
                                         (:const blpapi-identity-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-create-topics-async
                              "blpapi_ProviderSession_createTopicsAsync"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (topic-list
                               (:pointer (:const blpapi-topic-list-t)))
                              (resolve-mode :int)
                              (identity (:pointer
                                         (:const blpapi-identity-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-get-topic
                              "blpapi_ProviderSession_getTopic"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (message
                               (:pointer (:const blpapi-message-t)))
                              (topic
                               (:reference-return (:pointer blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-create-topic
                              "blpapi_ProviderSession_createTopic"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (message
                               (:pointer (:const blpapi-message-t)))
                              (topic
                               (:reference-return (:pointer blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-create-service-status-topic
                              "blpapi_ProviderSession_createServiceStatusTopic"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (service
                               (:pointer (:const blpapi-service-t)))
                              (topic
                               (:reference-return (:pointer blpapi-topic-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-delete-topics
                              "blpapi_ProviderSession_deleteTopics"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (topics
                               (:reference-return
                                (:pointer (:const blpapi-topic-t))))
                              (num-topics size-t))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-terminate-subscriptions-on-topics
                              "blpapi_ProviderSession_terminateSubscriptionsOnTopics"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (topics
                               (:reference-return
                                (:pointer (:const blpapi-topic-t))))
                              (num-topics size-t)
                              (message (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-publish
                              "blpapi_ProviderSession_publish"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (event (:pointer blpapi-event-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-send-response
                              "blpapi_ProviderSession_sendResponse"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t))
                              (event (:pointer blpapi-event-t))
                              (is-partial-response :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-provider-session-get-abstract-session
                              "blpapi_ProviderSession_getAbstractSession"
                              :source)
                             ((session
                               (:pointer blpapi-provider-session-t)))
                             :result-type
                             (:pointer blpapi-abstract-session-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-create
                              "blpapi_ServiceRegistrationOptions_create"
                              :source)
                             nil
                             :result-type
                             (:pointer
                              blpapi-service-registration-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-duplicate
                              "blpapi_ServiceRegistrationOptions_duplicate"
                              :source)
                             ((parameters
                               (:pointer
                                (:const
                                 blpapi-service-registration-options-t))))
                             :result-type
                             (:pointer
                              blpapi-service-registration-options-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-destroy
                              "blpapi_ServiceRegistrationOptions_destroy"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-copy
                              "blpapi_ServiceRegistrationOptions_copy"
                              :source)
                             ((lhs
                               (:pointer
                                blpapi-service-registration-options-t))
                              (rhs
                               (:pointer
                                (:const
                                 blpapi-service-registration-options-t))))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-add-active-sub-service-code-range
                              "blpapi_ServiceRegistrationOptions_addActiveSubServiceCodeRange"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t))
                              (start :int)
                              (end :int)
                              (priority :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-remove-all-active-sub-service-code-ranges
                              "blpapi_ServiceRegistrationOptions_removeAllActiveSubServiceCodeRanges"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-set-group-id
                              "blpapi_ServiceRegistrationOptions_setGroupId"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t))
                              (group-id (:reference :ef-mb-string :allow-null t))
                              (group-id-length (:unsigned :int)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-set-service-priority
                              "blpapi_ServiceRegistrationOptions_setServicePriority"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t))
                              (priority :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-set-parts-to-register
                              "blpapi_ServiceRegistrationOptions_setPartsToRegister"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t))
                              (parts :int))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-get-group-id
                              "blpapi_ServiceRegistrationOptions_getGroupId"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t))
                              (groupd-id-buffer (:pointer :char))
                              (group-id-length (:pointer :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-get-service-priority
                              "blpapi_ServiceRegistrationOptions_getServicePriority"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-service-registration-options-get-parts-to-register
                              "blpapi_ServiceRegistrationOptions_getPartsToRegister"
                              :source)
                             ((parameters
                               (:pointer
                                blpapi-service-registration-options-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_requesttemplate.h"

(fli:define-foreign-function (blpapi-request-template-add-ref
                              "blpapi_RequestTemplate_addRef"
                              :source)
                             ((request-template
                               (:pointer
                                (:const blpapi-request-template-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-request-template-release
                              "blpapi_RequestTemplate_release"
                              :source)
                             ((request-template
                               (:pointer
                                (:const blpapi-request-template-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_session.h"

(fli:define-c-typedef (blpapi-event-handler-t
                       (:foreign-name "blpapi_EventHandler_t"))
                      (:pointer
                       (:function
                        ((:pointer blpapi-event-t)
                         (:pointer blpapi-session-t)
                         (:pointer :void))
                        :void
                        :calling-convention
                        :cdecl)))
(fli:define-foreign-function (blpapi-session-create
                              "blpapi_Session_create"
                              :source)
                             ((parameters
                               (:pointer blpapi-session-options-t))
                              (handler blpapi-event-handler-t)
                              (dispatcher
                               (:pointer blpapi-event-dispatcher-t))
                              (user-data (:pointer :void)))
                             :result-type
                             (:pointer blpapi-session-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-destroy
                              "blpapi_Session_destroy"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-start
                              "blpapi_Session_start"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-start-async
                              "blpapi_Session_startAsync"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-stop
                              "blpapi_Session_stop"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-stop-async
                              "blpapi_Session_stopAsync"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-next-event
                              "blpapi_Session_nextEvent"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (event-pointer
                               (:reference-return (:pointer blpapi-event-t)))
                              (timeout-in-milliseconds
                               (:unsigned :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-try-next-event
                              "blpapi_Session_tryNextEvent"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (event-pointer
                               (:reference-return (:pointer blpapi-event-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-subscribe
                              "blpapi_Session_subscribe"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (subscription-list
                               (:pointer
                                (:const blpapi-subscription-list-t)))
                              (handle
                               (:pointer (:const blpapi-identity-t)))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-resubscribe
                              "blpapi_Session_resubscribe"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (resubscription-list
                               (:pointer
                                (:const blpapi-subscription-list-t)))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-resubscribe-with-id
                              "blpapi_Session_resubscribeWithId"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (resubscription-list
                               (:pointer
                                (:const blpapi-subscription-list-t)))
                              (resubscription-id :int)
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-unsubscribe
                              "blpapi_Session_unsubscribe"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (unsubscription-list
                               (:pointer
                                (:const blpapi-subscription-list-t)))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-cancel
                              "blpapi_Session_cancel"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (correlation-ids
                               (:pointer
                                (:const blpapi-correlation-id-t)))
                              (num-correlation-ids size-t)
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-set-status-correlation-id
                              "blpapi_Session_setStatusCorrelationId"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (service
                               (:pointer (:const blpapi-service-t)))
                              (identity (:pointer
                                         (:const blpapi-identity-t)))
                              (correlation-id
                               (:pointer
                                (:const blpapi-correlation-id-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-send-request
                              "blpapi_Session_sendRequest"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (request
                               (:pointer (:const blpapi-request-t)))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (identity (:pointer blpapi-identity-t))
                              (event-queue
                               (:pointer blpapi-event-queue-t))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-send-request-template
                              "blpapi_Session_sendRequestTemplate"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (request-template
                               (:pointer
                                (:const blpapi-request-template-t)))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-create-snapshot-request-template
                              "blpapi_Session_createSnapshotRequestTemplate"
                              :source)
                             ((request-template
                               (:reference-return
                                (:pointer blpapi-request-template-t)))
                              (session (:pointer blpapi-session-t))
                              (subscription-string
                               (:reference :ef-mb-string :allow-null t))
                              (identity (:pointer
                                         (:const blpapi-identity-t)))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-send-authorization-request
                              "blpapi_Session_sendAuthorizationRequest"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (request
                               (:pointer (:const blpapi-request-t)))
                              (identity (:pointer blpapi-identity-t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (event-queue
                               (:pointer blpapi-event-queue-t))
                              (request-label (:reference :ef-mb-string :allow-null t))
                              (request-label-len :int))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-open-service
                              "blpapi_Session_openService"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-open-service-async
                              "blpapi_Session_openServiceAsync"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (service-name (:reference :ef-mb-string :allow-null t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-generate-token
                              "blpapi_Session_generateToken"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (event-queue
                               (:pointer blpapi-event-queue-t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-get-service
                              "blpapi_Session_getService"
                              :source)
                             ((session (:pointer blpapi-session-t))
                              (service
                               (:reference-return (:pointer blpapi-service-t)))
                              (service-name (:reference :ef-mb-string :allow-null t)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-create-user-handle
                              "blpapi_Session_createUserHandle"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             (:pointer blpapi-user-handle-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-create-identity
                              "blpapi_Session_createIdentity"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             (:pointer blpapi-identity-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-session-get-abstract-session
                              "blpapi_Session_getAbstractSession"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             (:pointer blpapi-abstract-session-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-itr-create
                              "blpapi_SubscriptionItr_create"
                              :source)
                             ((session (:pointer blpapi-session-t)))
                             :result-type
                             (:pointer blpapi-subscription-iterator-t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-itr-destroy
                              "blpapi_SubscriptionItr_destroy"
                              :source)
                             ((iterator
                               (:pointer
                                blpapi-subscription-iterator-t)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-itr-next
                              "blpapi_SubscriptionItr_next"
                              :source)
                             ((iterator
                               (:pointer
                                blpapi-subscription-iterator-t))
                              (subscription-string
                               (:pointer (:reference :ef-mb-string :allow-null t)))
                              (correlation-id
                               (:pointer blpapi-correlation-id-t))
                              (status (:pointer :int)))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-subscription-itr-is-valid
                              "blpapi_SubscriptionItr_isValid"
                              :source)
                             ((iterator
                               (:pointer
                                (:const
                                 blpapi-subscription-iterator-t))))
                             :result-type
                             :int
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)

;;; Derived from file : "c:\\users\\cun\\quicklisp\\local-projects\\bloomie\\ffi\\blpapi-3.11.1.1/blpapi_versioninfo.h"

(fli:define-foreign-function (blpapi-get-version-info
                              "blpapi_getVersionInfo"
                              :source)
                             ((major-version (:pointer :int))
                              (minor-version (:pointer :int))
                              (patch-version (:pointer :int))
                              (build-version (:pointer :int)))
                             :result-type
                             :void
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
(fli:define-foreign-function (blpapi-get-version-identifier
                              "blpapi_getVersionIdentifier"
                              :source)
                             nil
                             :result-type
                             (:reference :ef-mb-string :allow-null t)
                             :language
                             :ansi-c
                             :calling-convention
                             :cdecl)
