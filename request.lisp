(in-package :bloomie)

#+lispworks
(defun create-request (&optional (type "ReferenceDataRequest") (service :refdata))
  (assert *session*)
  (multiple-value-bind (res request)
      (bb.ffi::blpapi-service-create-request (cdr (assoc service *services*)) nil type)
    (check-errors res)
    request))

#-lispworks
(defun create-request (&optional (type "ReferenceDataRequest") (service :refdata))
  (assert *session*)
  (plus-c:c-with ((request :pointer))
    (check-errors (bb.ffi:blpapi-service-create-request (cdr (assoc service *services*)) (request plus-c:&) type))
    (autowrap:wrap-pointer request 'bb.ffi:blpapi-request-t)))

#+lispworks
(defun request (type &key securities fields start-date end-date periodicity overrides
		       security event-type interval start-date-time end-date-time
		       screen-name screen-type group language-id
		       ids search field-type return-documentation
		       query yellow-key language-override max-results ticker partial-match
		       country-code currency-code curve-type curve-subtype curve-id bbg-id)
  (ensure-session)
  (let* ((service (cond ((member type '("ReferenceData" "HistoricalData" "IntradayBar" "PortfolioData" "Beqs") :test #'string=)
			 :refdata)
			((member type '("FieldList" "FieldInfo" "FieldSearch" "CategorizedFieldSearch") :test #'string=)
			 :apiflds)
			((member type '("curveList" "govtList" "instrumentList") :test #'string=)
			 :instruments)
			(t (error "don't know how to create a request of type ~A" type))))
	 (request (create-request (concatenate 'string type "Request") service))
	 (elements (bb.ffi::blpapi-request-elements request)))
    ;; ReferenceData, HistoricalData, PortfolioData
    (when securities (set-element elements "securities" (ensure-list securities)))
    (when fields (set-element elements "fields" (ensure-list fields)))
    ;; IntradayBar
    (when security (bb.ffi::blpapi-element-set-element-string elements "security" nil security))
    (when event-type (bb.ffi::blpapi-element-set-element-string elements "eventType" nil event-type))
    (when interval (bb.ffi::blpapi-element-set-element-int32 elements "interval" nil interval))
    (when start-date-time (bb.ffi::blpapi-element-set-element-string elements "startDateTime" nil start-date-time))
    (when end-date-time (bb.ffi::blpapi-element-set-element-string elements "endDateTime" nil end-date-time))
    ;; Beqs
    (when screen-name (bb.ffi::blpapi-element-set-element-string elements "screenName" nil screen-name))
    (when screen-type (bb.ffi::blpapi-element-set-element-string elements "screenType" nil screen-type))
    (when language-id (bb.ffi::blpapi-element-set-element-string elements "languageId" nil language-id))
    (when group (bb.ffi::blpapi-element-set-element-string elements "Group" nil group))
    ;; HistoricalData
    (when start-date (bb.ffi::blpapi-element-set-element-string elements "startDate" nil (bbdate start-date)))
    (when end-date (bb.ffi::blpapi-element-set-element-string elements "endDate" nil (bbdate end-date)))
    (when periodicity (bb.ffi::blpapi-element-set-element-string elements "periodicitySelection" nil (symbol-name periodicity)))
    ;; instrumentList, govtList, curveList
    (when query (bb.ffi::blpapi-element-set-element-string elements "query" nil query))
    (when max-results (bb.ffi::blpapi-element-set-element-int32 elements "maxResults" nil max-results))
    ;; instrumentList, govtList
    (when yellow-key (bb.ffi::blpapi-element-set-element-string elements "yellowKeyFilter" nil yellow-key))
    ;; instrumentList
    (when language-override (bb.ffi::blpapi-element-set-element-string elements "languageOverride" nil language-override))
    ;; govtList
    (when ticker (bb.ffi::blpapi-element-set-element-string elements "ticker" nil ticker))
    (when partial-match (bb.ffi::blpapi-element-set-element-bool elements "partialMatch" nil 1))
    ;; curveList
    (when country-code (bb.ffi::blpapi-element-set-element-string elements "countryCode" nil country-code))
    (when currency-code (bb.ffi::blpapi-element-set-element-string elements "currencyCode" nil currency-code))
    (when curve-type (bb.ffi::blpapi-element-set-element-string elements "type" nil curve-type))
    (when curve-subtype (bb.ffi::blpapi-element-set-element-string elements "subtype" nil curve-subtype))
    (when curve-id (bb.ffi::blpapi-element-set-element-string elements "curveid" nil curve-id))
    (when bbg-id (bb.ffi::blpapi-element-set-element-string elements "bbgid" nil bbg-id))
    ;; FieldInfo
    (when ids (set-element elements "id" (ensure-list ids)))
    ;; FieldSearch, CategorizedFieldSearch
    (when search (bb.ffi::blpapi-element-set-element-string elements "searchSpec" nil search))
    ;; FieldList
    (when field-type (bb.ffi::blpapi-element-set-element-string elements "fieldType" nil field-type))
    ;; FieldInfo, FieldSearch, CategorizedFieldSearch
    (when return-documentation (bb.ffi::blpapi-element-set-element-bool elements "returnFieldDocumentation" nil 1))
    ;; Overrides
    (when overrides 
      (let ((o (get-element elements "overrides")))
	(loop for (field value) in overrides
	   do (let ((new (append-element o)))
		(bb.ffi::blpapi-element-set-element-string new "fieldId" nil field)
		(bb.ffi::blpapi-element-set-element-string new "value" nil value)))))
    request))

#-lispworks
(defun request (type &key securities fields start-date end-date periodicity overrides
		       security event-type interval start-date-time end-date-time
		       screen-name screen-type group language-id
		       ids search field-type return-documentation
		       query yellow-key language-override max-results ticker partial-match
		       country-code currency-code curve-type curve-subtype curve-id bbg-id)
  (ensure-session)
  (let* ((service (cond ((member type '("ReferenceData" "HistoricalData" "IntradayBar" "Beqs" "PortfolioData" "Ping") :test #'string=)
			 :refdata)
			((member type '("FieldList" "FieldInfo" "FieldSearch" "CategorizedFieldSearch") :test #'string=)
			 :apiflds)
			((member type '("curveList" "govtList" "instrumentList") :test #'string=)
			 :instruments)
			(t (error "don't know how to create a request of type ~A" type))))
	 (request (create-request (concatenate 'string type "Request") service))
	 (elements (bb.ffi:blpapi-request-elements request)))
    ;; ReferenceData, HistoricalData, PortfolioData
    (when securities (set-element elements "securities" (ensure-list securities)))
    (when fields (set-element elements "fields" (ensure-list fields)))
    ;; IntradayBar
    (when security (bb.ffi:blpapi-element-set-element-string elements "security" nil security))
    (when event-type (bb.ffi:blpapi-element-set-element-string elements "eventType" nil event-type))
    (when interval (bb.ffi:blpapi-element-set-element-int32 elements "interval" nil interval))
    (when start-date-time (bb.ffi:blpapi-element-set-element-string elements "startDateTime" nil start-date-time))
    (when end-date-time (bb.ffi:blpapi-element-set-element-string elements "endDateTime" nil end-date-time))
    ;; Beqs
    (when screen-name (bb.ffi:blpapi-element-set-element-string elements "screenName" nil screen-name))
    (when screen-type (bb.ffi:blpapi-element-set-element-string elements "screenType" nil screen-type))
    (when language-id (bb.ffi:blpapi-element-set-element-string elements "languageId" nil language-id))
    (when group (bb.ffi:blpapi-element-set-element-string elements "Group" nil group))
    ;; HistoricalData
    (when start-date (bb.ffi:blpapi-element-set-element-string elements "startDate" nil (bbdate start-date)))
    (when end-date (bb.ffi:blpapi-element-set-element-string elements "endDate" nil (bbdate end-date)))
    (when periodicity (bb.ffi:blpapi-element-set-element-string elements "periodicitySelection" nil (symbol-name periodicity)))
    ;; instrumentList, govtList, curveList
    (when query (bb.ffi:blpapi-element-set-element-string elements "query" nil query))
    (when max-results (bb.ffi:blpapi-element-set-element-int32 elements "maxResults" nil max-results))
    ;; instrumentList, govtList
    (when yellow-key (bb.ffi:blpapi-element-set-element-string elements "yellowKeyFilter" nil yellow-key))
    ;; instrumentList
    (when language-override (bb.ffi:blpapi-element-set-element-string elements "languageOverride" nil language-override))
    ;; govtList
    (when ticker (bb.ffi:blpapi-element-set-element-string elements "ticker" nil ticker))
    (when partial-match (bb.ffi:blpapi-element-set-element-bool elements "partialMatch" nil 1))
    ;; curveList
    (when country-code (bb.ffi:blpapi-element-set-element-string elements "countryCode" nil country-code))
    (when currency-code (bb.ffi:blpapi-element-set-element-string elements "currencyCode" nil currency-code))
    (when curve-type (bb.ffi:blpapi-element-set-element-string elements "type" nil curve-type))
    (when curve-subtype (bb.ffi:blpapi-element-set-element-string elements "subtype" nil curve-subtype))
    (when curve-id (bb.ffi:blpapi-element-set-element-string elements "curveid" nil curve-id))
    (when bbg-id (bb.ffi:blpapi-element-set-element-string elements "bbgid" nil bbg-id))
    ;; FieldInfo
    (when ids (set-element elements "id" (ensure-list ids)))
    ;; FieldSearch, CategorizedFieldSearch
    (when search (bb.ffi:blpapi-element-set-element-string elements "searchSpec" nil search))
    ;; FieldList
    (when field-type (bb.ffi:blpapi-element-set-element-string elements "fieldType" nil field-type))
    ;; FieldList, FieldInfo, FieldSearch, CategorizedFieldSearch
    (when return-documentation (bb.ffi:blpapi-element-set-element-bool elements "returnFieldDocumentation" nil 1))
    ;; Overrides
    (when overrides
      (let ((o (get-element elements "overrides")))
	(loop for (field value) in overrides
	   do (let ((new (append-element o)))
		(bb.ffi:blpapi-element-set-element-string new "fieldId" (null-pointer) field)
		(bb.ffi:blpapi-element-set-element-string new "value" (null-pointer) value)))))
    request))

(defvar *request-id* 0)

#+lispworks
(defun send-request (request &optional (id (incf *request-id*)))
  (assert *session*)
  (fli:with-dynamic-foreign-objects ((correlationid bb.ffi::blpapi-correlation-id-t :fill 0))
    (setf (fli:foreign-slot-value correlationid 'bb.ffi::size) (fli:size-of 'bb.ffi::blpapi-correlation-id-t)
	  (fli:foreign-slot-value correlationid 'bb.ffi::value-type) 1) ;; bb.ffi::+blpapi-correlation-type-int+
    ;; This doesn't work (No implementation of :raw-64), a work-around follows
    ;; (setf (fli:foreign-slot-value (fli:foreign-slot-pointer correlationid 'bb.ffi::value) 'bb.ffi::int-value) id)
    (setf (fli:dereference (fli:foreign-slot-pointer (fli:foreign-slot-pointer correlationid 'bb.ffi::value) 'bb.ffi::int-value)
			   :type :unsigned-int) id)
    (check-errors (bb.ffi::blpapi-session-send-request *session* request correlationid
						       (null-pointer 'bb.ffi::blpapi-identity-t)
						       (null-pointer 'bb.ffi::blpapi-event-queue-t)
						       nil 0)))
  (bb.ffi::blpapi-request-destroy request)
  id)

#-lispworks
(defun send-request (request &optional (id (incf *request-id*)))
  (assert *session*)
  (autowrap:with-alloc (correlationid 'bb.ffi:blpapi-correlation-id-t)
    (let ((size (autowrap:sizeof 'bb.ffi:blpapi-correlation-id-t)))
      (dotimes (i size)
	(setf (cffi:mem-aref (autowrap:ptr correlationid) :unsigned-char i) 0))
      (setf (bb.ffi:blpapi-correlation-id-t.size correlationid) size
	    (bb.ffi:blpapi-correlation-id-t.value-type correlationid) bb.ffi:+blpapi-correlation-type-int+
	    (bb.ffi:blpapi-correlation-id-t.value.int-value correlationid) id))
    (check-errors (bb.ffi:blpapi-session-send-request *session* request correlationid
						      (null-pointer) (null-pointer)
						      (null-pointer) 0)))
  (bb.ffi::blpapi-request-destroy request)
  id)
