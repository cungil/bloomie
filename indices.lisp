(in-package :bloomie)

(defun index-weights (index)
  (let ((raw (bdp (concatenate 'string index " Index") "INDX_MWEIGHT")))
    (if (second (first raw))
	(unify:match ('((?index
			 #T(list :INDX_MWEIGHT
			    &rest ?members)))
		       raw)
	  (loop for member in members
	     collect (unify:match ('(:INDX_MWEIGHT
				     (:|Member Ticker and Exchange Code| ?stock)
				     (:|Percentage Weight| ?weight))
				    member)
		       (cons stock (if (= weight *na-double*) nil weight)))))
	(error (third (first raw))))))

(defun index-weights-asof (index date)
  (let ((raw (bdp (concatenate 'string index " Index") "INDX_MWEIGHT_HIST"
		  :overrides `(("END_DT" ,(bbdate date))))))
    (if (second (first raw))
	(unify:match ('((?index
			 #T(list :INDX_MWEIGHT_HIST
			    &rest ?members)))
		       raw)
	  (loop for member in members
	     collect (unify:match ('(:INDX_MWEIGHT_HIST
				     (:|Index Member| ?stock)
				     (:|Percent Weight| ?weight))
				    member)
		     (cons stock (if (= weight *na-double*) nil weight)))))
	(error (third (first raw))))))

(defun index-members (index)
  (let ((raw (bdp (concatenate 'string index " Index") "INDX_MEMBERS")))
    (if (second (first raw))
	(let ((members (unify:match ('((?index
					#T(list :INDX_MEMBERS
					   &rest ?members)))
				      raw)
			 (loop for member in members
			    collect (unify:match ('(:INDX_MEMBERS (:|Member Ticker and Exchange Code| ?stock)) 
						   member)
				      stock)))))
	  (if (= (length members) 2500)
	      (append members (unify:match ('((?index
					       #T(list :INDX_MEMBERS2
						  &rest ?members)))
					     (bdp (concatenate 'string index " Index") "INDX_MEMBERS2"))
				(loop for member in members
				   collect (unify:match ('(:INDX_MEMBERS2 (:|Member Ticker and Exchange Code| ?stock)) 
							  member)
					     stock))))
	      members))
	(error (third (first raw))))))

(defun date-oldest-composition (index)
  (second (second (first (bdp (concatenate 'string index " Index")
			      "INDEX_MEMBERSHIP_MAINT_DATE")))))

(defvar *composite-exchanges*
  '((:UA . :US) (:UN . :US)(:UQ . :US) (:UR . :US) (:UW . :US)
    (:KP . :KS) (:KQ . :KS)
    (:SM . :SM) (:SQ . :SM)
    (:AT . :AU)
    (:BS . :BZ)
    (:CC . :CI)
    (:CT . :CN)
    (:CK . :CP)
    (:GY . :GR)
    (:JT . :JP)
    (:SE . :SW)
    ;; not really
    (:AV . :AV) (:BB . :BB) (:DC . :DC) (:EB . :EB) (:FH . :FH) (:FP . :FP) (:GA . :GA)
    (:HK . :HK) (:ID . :ID) (:IM . :IM) (:LN . :LN) (:MM . :MM) (:NA . :NA) (:NO . :NO)
    (:NQ . :NQ) (:PL . :PL) (:QM . :QM) (:SP . :SP) (:SS . :SS) (:TT . :TT) (:VX . :VX)))
 
(defun composite-ticker (ticker)
  (let* ((exchange (intern (subseq ticker (- (length ticker) 2)) "KEYWORD"))
	 (composite (cdr (assoc exchange *composite-exchanges*))))
    (if composite
	(format nil "~A ~A" (subseq ticker 0 (- (length ticker) 3)) composite)
	(destructuring-bind (original composite)
	    (mapcar #'second
		    (second (first (bdp (concatenate 'string ticker " Equity")
					'("EXCH_CODE" "COMPOSITE_EXCH_CODE")))))
	  (assert (string= (subseq ticker (- (length ticker) 3))
			   (concatenate 'string " " original)))
	  (push (cons (intern original "KEYWORD") (intern composite "KEYWORD")) *composite-exchanges*)
	  (warn "you might want to add this pair to *composite-exchanges*: ~S"
		(cons (intern original "KEYWORD") (intern composite "KEYWORD")))
	  (composite-ticker ticker)))))

(defun index-composition (index &key date weights (composite-exchange t))
  "Retrieve the list of components for an index, for the given date if one is supplied.
If the flag :weight is set to true, it returns pairs (ticker . weight) instead of tickers."
  (when date
    (let ((oldest-available (date-oldest-composition index)))
      (when (null oldest-available) (error "the composition is not available for index ~A" index))
      (when (minusp (local-time:timestamp-difference (local-time:parse-timestring date) (local-time:parse-timestring oldest-available)))
	(error "composition of index ~A is not available before ~A"
	       index (local-time:format-rfc3339-timestring nil oldest-available :omit-time-part t)))))
  (let ((result (cond ((and weights date) (index-weights-asof index date))
		      ((and weights (not date)) (index-weights index))
		      ((and (not weights) date) (mapcar #'car (index-weights-asof index date))) ;; historical data not available for INDX_MEMBERS
		      ((and (not weights) (not date)) (index-members index)))))
    (if composite-exchange
	(if weights
	    (loop for (ticker . weight) in result collect (cons (composite-ticker ticker) weight))
	    (mapcar #'composite-ticker result))
	result)))
