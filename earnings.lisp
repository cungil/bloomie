(in-package :bloomie)

(defstruct (eps-announcement (:conc-name eps-))
  period date time reported comparable estimated)

(defun get-earnings (ticker &key start end currency)
  "Returns the fiscal period, date, time and reported/comparable/estimated EPS 
for earnings announcements in the given period. The currency can also be set."
  (loop for (period date time reported comparable estimated)
     in (second (second (first (bdp (format nil "~A Equity" ticker)
				    '("EARN_ANN_DT_TIME_HIST_WITH_EPS")
				    :overrides (append
						(when start `(("START_DT" ,(bbdate start))))
						(when end `(("END_DT" ,(bbdate end))))
						(when currency `(("EQY_FUND_CRNCY" ,currency))))))))
     collect (make-eps-announcement :period period :date date :time time
				    :reported reported :comparable comparable :estimated estimated)))

(defun get-earnings-dates (ticker)
  "Returns (date fiscal-period) pairs for the whole history of earnings announcements."
  (second (second (first (bdp (format nil "~A Equity" ticker) '("ERN_ANN_DT_AND_PER"))))))

