(in-package :bloomie)

(defvar *data-types*
  (loop for s in (apropos-list "+BLPAPI-DATATYPE-" "BB.FFI")
     collect (cons (symbol-value s) (intern (subseq (string-trim "+" (symbol-name s)) (length "BLPAPI-DATATYPE-")) "KEYWORD"))))

#+lispworks
(setf *data-types* '((1 . :BOOL) (3 . :BYTE) (9 . :BYTEARRAY) (2 . :CHAR)
		     (16 . :CHOICE) (17 . :CORRELATION-ID) (10 . :DATE)
		     (13 . :DATETIME) (12 . :DECIMAL) (14 . :ENUMERATION)
		     (6 . :FLOAT32) (7 . :FLOAT64) (4 . :INT32) (5 . :INT64)
		     (15 . :SEQUENCE) (8 . :STRING) (11 . :TIME)))

(defun decode-data-type (x)
  (or (cdr (assoc x *data-types*)) (error "data-type ~A not in ~A" x *data-types*)))

#+lispworks
(defun get-element (elements name)
  (multiple-value-bind (res element)
      (bb.ffi::blpapi-element-get-element elements nil name nil)
    (check-errors res)
    element))

#-lispworks
(defun get-element (elements name)
  (plus-c:c-with ((element :pointer))
    (check-errors (bb.ffi:blpapi-element-get-element elements (element plus-c:&) name nil))
    (autowrap:wrap-pointer element 'bb.ffi:blpapi-element-t)))

#+lispworks
(defun set-element (elements name values)
  (let ((s (get-element elements name)))
    (dolist (val values)
      (check-errors (bb.ffi::blpapi-element-set-value-string
		     s val (fli:cast-integer -1 '(:unsigned :int)))))))

#-lispworks
(defun set-element (elements name values)
  ;;; bb.ffi:+blpapi-element-index-end+ is -1, but I cannot pass this value as :unsigned-int
  ;;; (cffi:with-foreign-object (x :unsigned-int) (setf (cffi:mem-aref x :int) -1) (cffi:mem-aref x :unsigned-int))
  (let ((s (get-element elements name)))
    (dolist (val values)
      (check-errors (bb.ffi:blpapi-element-set-value-string s val 4294967295)))))

#+lispworks
(defun append-element (elements)
  (multiple-value-bind (res element)
      (bb.ffi::blpapi-element-append-element elements nil)
    (check-errors res)
    element))

#-lispworks
(defun append-element (elements)
  (plus-c:c-with ((element :pointer))
    (check-errors (bb.ffi:blpapi-element-append-element elements (element plus-c:&)))
    (autowrap:wrap-pointer element 'bb.ffi:blpapi-element-t)))

#+lispworks
(defun get-element-at (elements i)
  (multiple-value-bind (res val)
      (bb.ffi::blpapi-element-get-element-at elements nil i)
    (check-errors res)
    val))

#-lispworks
(defun get-element-at (elements i)
  (plus-c:c-with ((val :pointer))
    (check-errors (bb.ffi:blpapi-element-get-element-at elements (val plus-c:&) i))
    val))

#+lispworks
(defun get-choice (element)
  (multiple-value-bind (res choice)
      (bb.ffi::blpapi-element-get-choice element nil)
    (check-errors res)
    choice))

#-lispworks
(defun get-choice (element)
  (plus-c:c-with ((choice :pointer))
    (check-errors (bb.ffi:blpapi-element-get-choice element (choice plus-c:&)))
    choice))

(defun element-info (element)
  (list (bb.ffi::blpapi-element-name-string element)
	:datatype (decode-data-type (bb.ffi::blpapi-element-datatype element))
	:isarray (bb.ffi::blpapi-element-is-array element)
	:nelements (bb.ffi::blpapi-element-num-elements element)
	:iscomplex (bb.ffi::blpapi-element-is-complex-type element)
	:nvalues (bb.ffi::blpapi-element-num-values element)
	:isreadonly (bb.ffi::blpapi-element-is-read-only element)
	:isnull (bb.ffi::blpapi-element-is-null element)
	:schema (decode-schema element)))

(defun decode-type-definition (def)
  (list :name (name-string-and-destroy (bb.ffi::blpapi-schema-type-definition-name def))
	:description (bb.ffi::blpapi-schema-type-definition-description def)
	:datatype (decode-data-type (bb.ffi::blpapi-schema-type-definition-datatype def))))

(defun decode-schema (element)
  (let ((def (bb.ffi::blpapi-element-definition element)))
    (list :name (name-string-and-destroy (bb.ffi::blpapi-schema-element-definition-name def))
	  :alt-names (let ((nalternative (bb.ffi::blpapi-schema-element-definition-num-alternate-names def)))
		       (if (plusp nalternative)
			   (loop for i below nalternative
			      collect (name-string-and-destroy
				       (bb.ffi::blpapi-schema-element-definition-get-alternate-name def i)))))
	  :description (bb.ffi::blpapi-schema-element-definition-description def)
	  :type (decode-type-definition (bb.ffi::blpapi-schema-element-definition-type def))
	  :status (bb.ffi::blpapi-schema-element-definition-status def)
	  :min-values (bb.ffi::blpapi-schema-element-definition-min-values def)
	  :max-values (bb.ffi::blpapi-schema-element-definition-max-values def))))

(defun decode-element (element)
  (assert (or (zerop (bb.ffi::blpapi-element-is-array element))
	      (zerop (bb.ffi::blpapi-element-is-complex-type element))))
  (assert (string= (name-string-and-destroy (bb.ffi::blpapi-element-name element))
  		   (bb.ffi::blpapi-element-name-string element)))
  (cons (intern (bb.ffi::blpapi-element-name-string element) "KEYWORD")
	(ecase (decode-data-type (bb.ffi::blpapi-element-datatype element))
	  (:date (loop for i below (bb.ffi::blpapi-element-num-values element)
		    collect (value-as-date element i)))
	  (:datetime (loop for i below (bb.ffi::blpapi-element-num-values element)
			collect (value-as-date element i t)))
	  (:bool (loop for i below (bb.ffi::blpapi-element-num-values element)
		    collect (value-as-bool element i)))
	  (:int32 (loop for i below (bb.ffi::blpapi-element-num-values element)
		     collect (value-as-int32 element i)))
	  (:int64 (loop for i below (bb.ffi::blpapi-element-num-values element)
		     collect (value-as-int64 element i)))
	  (:float64 (loop for i below (bb.ffi::blpapi-element-num-values element)
		       collect (value-as-float64 element i)))
	  (:string (loop for i below (bb.ffi::blpapi-element-num-values element)
		      collect (value-as-string element i)))
	  (:sequence (if (= 1 (bb.ffi::blpapi-element-is-array element))
			 (loop for i below (bb.ffi::blpapi-element-num-values element)
			    collect (decode-element (value-as-element element i)))
			 (loop for i below (bb.ffi::blpapi-element-num-elements element)
			    collect (decode-element (get-element-at element i)))))
	  (:enumeration (list (value-as-string element 0)))
	  (:choice (decode-element (get-choice element))))))
