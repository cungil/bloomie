(in-package :bloomie)

(defvar *session* nil)

(defvar *services* nil)

(defun list-session-options ()
  (loop for s in (apropos-list "BLPAPI-SESSION-OPTIONS-SET-" "BB.FFI")
     collect (intern (subseq (symbol-name s) (length "BLPAPI-SESSION-OPTIONS-SET-")) "KEYWORD")))

#+lispworks
(defun get-service (name)
  (assert *session*)
  (let ((full-name (concatenate 'string "//blp/" (string-downcase (symbol-name name)))))
    (multiple-value-bind (res service)
	(bb.ffi::blpapi-session-get-service *session* nil full-name)
      (check-errors res)
      service)))

#-lispworks
(defun get-service (name)
  (assert *session*)
  (let ((full-name (concatenate 'string "//blp/" (string-downcase (symbol-name name)))))
    (plus-c:c-with ((service :pointer))
      (check-errors (bb.ffi:blpapi-session-get-service *session* (service plus-c:&) full-name))
      (autowrap:wrap-pointer service 'bb.ffi:blpapi-service-t))))

#+lispworks
(defun list-service-operations (service)
  (loop for idx below (bb.ffi::blpapi-service-num-operations service)
     collect (multiple-value-bind (res operation)
		 (bb.ffi::blpapi-service-get-operation-at service nil idx)
	       (check-errors res)
	       (bb.ffi::blpapi-operation-name operation))))

#-lispworks
(defun list-service-operations (service)
  (loop for idx below (bb.ffi:blpapi-service-num-operations service)
     collect (plus-c:c-with ((operation :pointer))
	       (check-errors (bb.ffi:blpapi-service-get-operation-at service (operation plus-c:&) idx))
	       (assert (string= "" (bb.ffi:blpapi-operation-description operation)))
	       (bb.ffi:blpapi-operation-name operation))))

#+lispworks
(defun schema-service-operations (service)
  (loop for idx below (bb.ffi::blpapi-service-num-operations service)
     do (multiple-value-bind (res operation)
	    (bb.ffi::blpapi-service-get-operation-at service nil idx)
	  (check-errors res)
	  (let ((operation-name (bb.ffi::blpapi-operation-name operation)))
	    (multiple-value-bind (res definition)
		(bb.ffi::blpapi-operation-request-definition operation nil)
	      (check-errors res)
	      (let ((request-name (name-string-and-destroy (bb.ffi::blpapi-schema-element-definition-name definition))))
		(with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A-~A" operation-name request-name)
								  (asdf:system-source-directory :bloomie))
						 :direction :output :if-exists :supersede)
		  (bb.ffi::blpapi-schema-element-definition-print definition (fli:make-pointer :symbol-name 'stream-writer) nil 0 2))))
	    (loop for i from 0 below (bb.ffi::blpapi-operation-num-response-definitions operation)
	       ;; INSTRUMENTS: 2   APIFLDS: 1   REFDATA: 0  ?  there are many missing!  for example PortfolioDataResponse
	       do (multiple-value-bind (res definition)
		      (bb.ffi::blpapi-operation-request-definition operation nil)
		    (check-errors res)
		    (let ((response-name (name-string-and-destroy (bb.ffi::blpapi-schema-element-definition-name definition))))
		      (with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A-~A" operation-name response-name)
									(asdf:system-source-directory :bloomie))
						       :direction :output :if-exists :supersede)
			(bb.ffi::blpapi-schema-element-definition-print definition (fli:make-pointer :symbol-name 'stream-writer) nil 0 2)))))))))

#-lispworks
(defun schema-service-operations (service)
  (loop for idx below (bb.ffi:blpapi-service-num-operations service)
     do (plus-c:c-with ((operation :pointer))
	  (check-errors (bb.ffi:blpapi-service-get-operation-at service (operation plus-c:&) idx))
	  (let ((operation-name (bb.ffi:blpapi-operation-name operation)))
	    (plus-c:c-with ((definition :pointer))
	      (check-errors (bb.ffi:blpapi-operation-request-definition operation (definition plus-c:&)))
	      (let ((request-name (name-string-and-destroy (bb.ffi:blpapi-schema-element-definition-name definition))))
		(with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A-~A" operation-name request-name)
								  (asdf:system-source-directory :bloomie))
						 :direction :output :if-exists :supersede)
		  (bb.ffi::blpapi-schema-element-definition-print definition (cffi:callback stream-writer) nil 0 2))))
	    (loop for i from 0 below (bb.ffi:blpapi-operation-num-response-definitions operation)
	       ;; INSTRUMENTS: 2   APIFLDS: 1   REFDATA: 0  ?  there are many missing!  for example PortfolioDataResponse
	       do (plus-c:c-with ((definition :pointer))
		    (check-errors (bb.ffi:blpapi-operation-response-definition operation (definition plus-c:&) i))
		    (let ((response-name (name-string-and-destroy (bb.ffi:blpapi-schema-element-definition-name definition))))
		      (with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A-~A" operation-name response-name)
									(asdf:system-source-directory :bloomie))
						       :direction :output :if-exists :supersede)
			(bb.ffi::blpapi-schema-element-definition-print definition (cffi:callback stream-writer) nil 0 2)))))))))

#+lispworks
(defun list-service-events (service)
  (loop for idx below (bb.ffi::blpapi-service-num-event-definitions service)
     collect (multiple-value-bind (res event)
		 (bb.ffi::blpapi-service-get-event-definition-at service nil idx)
	       (check-errors res)
	       (assert (string= "" (bb.ffi::blpapi-schema-element-definition-description event)))
	       (name-string-and-destroy (bb.ffi::blpapi-schema-element-definition-name event)))))

#-lispworks
(defun list-service-events (service)
  (loop for idx below (bb.ffi:blpapi-service-num-event-definitions service)
     collect (plus-c:c-with ((event :pointer))
	       (check-errors (bb.ffi:blpapi-service-get-event-definition-at service (event plus-c:&) idx))
	       (assert (string= "" (bb.ffi::blpapi-schema-element-definition-description event)))
	       (name-string-and-destroy (bb.ffi:blpapi-schema-element-definition-name event)))))

#+lispworks
(defun schema-service-events (service)
  (loop for idx below (bb.ffi::blpapi-service-num-event-definitions service)
     do (multiple-value-bind (res event)
		 (bb.ffi::blpapi-service-get-event-definition-at service nil idx)
	       (check-errors res)
	       (let ((name (name-string-and-destroy (bb.ffi::blpapi-schema-element-definition-name event))))
		 (with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A" name)
								   (asdf:system-source-directory :bloomie))
						  :direction :output :if-exists :supersede)
		   (bb.ffi::blpapi-schema-element-definition-print event (fli:make-pointer :symbol-name 'stream-writer) nil 0 2))))))

#-lispworks
(defun schema-service-events (service)
  (loop for idx below (bb.ffi:blpapi-service-num-event-definitions service)
     do (plus-c:c-with ((event :pointer))
	  (check-errors (bb.ffi:blpapi-service-get-event-definition-at service (event plus-c:&) idx))
	  (let ((name (name-string-and-destroy (bb.ffi:blpapi-schema-element-definition-name event))))
	    (with-open-file (*schema-stream* (merge-pathnames (format nil "schemas/~A" name)
							      (asdf:system-source-directory :bloomie))
					     :direction :output :if-exists :supersede)
	      (bb.ffi::blpapi-schema-element-definition-print event (cffi:callback stream-writer) nil 0 2))))))

(defun document-schemas ()
  "Write the schemas for requests, responses and events in the schemas/ directory.
There seems to be a bug and no response schemas can be retrieved for requests in the refdata service!"
  (loop for (name . svc) in *services*
     do (print name)
       (bb::schema-service-operations svc)
       (bb::schema-service-events svc)))

(defun open-service (name &optional get-service)
  (assert *session*)
  (let ((full-name (concatenate 'string "//blp/" (string-downcase (symbol-name name)))))
    (unless (zerop (bb.ffi::blpapi-session-open-service *session* full-name))
      (error "can't open service ~A" full-name)))
  (when get-service (let ((service (get-service name)))
		      (format t "~&service: ~A~&  operations: ~A~&  events: ~A" name
			      (or (list-service-operations service) "")
			      (or (list-service-events service) ""))
		      (setf *services* (cons (cons name service) *services*)))))

(defun new-session (&key server-host server-port default-subscription-service default-topic-prefix)
  (let ((options (bb.ffi::blpapi-session-options-create)))
    (when server-host (bb.ffi::blpapi-session-options-set-server-host options server-host))
    (when server-port (bb.ffi::blpapi-session-options-set-server-port options server-port))
    (when default-topic-prefix (bb.ffi::blpapi-session-options-set-default-topic-prefix options default-topic-prefix))
    (when default-subscription-service (bb.ffi::blpapi-session-options-set-default-subscription-service options default-subscription-service))
    (let ((session (bb.ffi::blpapi-session-create options
						  (null-pointer :function)
						  (bb.ffi::blpapi-event-dispatcher-create 0)
						  (null-pointer :function))))
      (bb.ffi::blpapi-session-options-destroy options)
      session)))

(defun open-session (&key host port
		       (services '(:refdata :apiflds :instruments
				   :mktdata :mktbar :mktvwap))
		       (default-subscription-service "//blp/mktdata")
		       (default-topic-prefix "/ticker/"))
  "Connect to Bloomberg, opening and listing all the services. If there is an active connection, 
there is an option to reuse it (a continuable error is raised)."
  (load-blpapi-library :if-loaded :ignore)
  (if *session*
      (cerror "return active session" "session already running!")
      (let ((session (new-session :server-host host :server-port port
				  :default-subscription-service default-subscription-service
				  :default-topic-prefix default-topic-prefix)))
	(check-errors (bb.ffi::blpapi-session-start session))
	(setf *session* session)
	(loop for service in services do (open-service service t))
	#+lispworks (retrieve-loop)))
  *session*)

(defun close-session ()
  "Disconnect from Bloomberg. If there is no active connection, prints a warning."
  (if (null *session*)
      (warn "session not running!")
      (progn
	#+NIL ;; if services are released, destroying the session results in a crash
	(loop for (name . service) in *services*
	   do (bb.ffi:blpapi-service-release service))
	(check-errors (bb.ffi::blpapi-session-stop *session*))
	(bb.ffi::blpapi-session-destroy *session*)
	(setf *session* nil
	      *services* nil))))

(defun ensure-session ()    
  (when (null *session*)
    (cerror "Run (open-session)" "No Bloomberg session open")
    (open-session)))
